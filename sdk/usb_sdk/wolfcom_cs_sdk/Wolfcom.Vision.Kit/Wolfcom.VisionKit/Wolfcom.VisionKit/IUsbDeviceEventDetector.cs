﻿using System;

namespace Wolfcom.VisionKit
{
    internal interface IUsbDeviceEventDetector
    {
        event EventHandler<EventArgs> DeviceInsertedOrRemoved;

        event EventHandler<DriveChangedEventArgs> DriveInserted;

        event EventHandler<DriveChangedEventArgs> DriveRemoved;
    }
}
