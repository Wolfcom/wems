﻿using System;

namespace Wolfcom.ThirdEyeKit
{
    public class ThirdEyeFileImportEventArgs : EventArgs
    {
        public readonly ulong CurrentLength;

        internal ThirdEyeFileImportEventArgs (ulong currentLength)
        {
            CurrentLength = currentLength;

        }
    }
}
