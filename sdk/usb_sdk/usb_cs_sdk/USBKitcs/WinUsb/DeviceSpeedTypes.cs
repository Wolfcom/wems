
namespace USBKitcs.WinUsb
{
    /// <summary> Device speed types
    /// </summary> 
    public enum DeviceSpeedTypes : byte
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Undefined = 0x00,
        /// <summary>
        /// Low speed device.
        /// </summary>
        LowSpeed = 0x01,
        /// <summary>
        /// Full speed device.
        /// </summary>
        FullSpeed = 0x02,
        /// <summary>
        /// High speed device.
        /// </summary>
        HighSpeed = 0x03,
    }
}