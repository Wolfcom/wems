﻿
namespace Wolfcom.ThirdEyeKit
{
    public enum VideoResolutions
    {
        Resolution1920x680,
        Resolution1280x720x60fps,
        Resolution1280x720x30fps,
        Resolution848x480x60fps,
        Resolution848x480x30fps,
        Resolution640x480,
    }
}
