
using System.Runtime.InteropServices;

using USBKitcs.Descriptors;
using USBKitcs.MonoLibUsb.Descriptors;

namespace USBKitcs.DeviceNotify.Linux
{
    internal class LinuxDevItem
    {
        public readonly byte BusNumber;
        public readonly byte DeviceAddress;
        public readonly UsbDeviceDescriptor DeviceDescriptor;
        public readonly string DeviceFileName;

        public LinuxDevItem(string deviceFileName, byte busNumber, byte deviceAddress, byte[] fileDescriptor)
        {
            DeviceFileName = deviceFileName;
            BusNumber = busNumber;
            DeviceAddress = deviceAddress;


            DeviceDescriptor = new UsbDeviceDescriptor();
            GCHandle gcFileDescriptor = GCHandle.Alloc(DeviceDescriptor, GCHandleType.Pinned);
            Marshal.Copy(fileDescriptor, 0, gcFileDescriptor.AddrOfPinnedObject(), Marshal.SizeOf(DeviceDescriptor));

            gcFileDescriptor.Free();
        }

        public LinuxDevItem(string deviceFileName, byte busNumber, byte deviceAddress, MonoUsbDeviceDescriptor monoUsbDeviceDescriptor)
        {
            DeviceFileName = deviceFileName;
            BusNumber = busNumber;
            DeviceAddress = deviceAddress;


            DeviceDescriptor = new UsbDeviceDescriptor(monoUsbDeviceDescriptor);
        }

        public bool Equals(LinuxDevItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.DeviceFileName, DeviceFileName) && other.BusNumber == BusNumber && other.DeviceAddress == DeviceAddress &&
                   Equals(other.DeviceDescriptor, DeviceDescriptor);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (LinuxDevItem)) return false;
            return Equals((LinuxDevItem) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = (DeviceFileName != null ? DeviceFileName.GetHashCode() : 0);
                result = (result*397) ^ BusNumber.GetHashCode();
                result = (result*397) ^ DeviceAddress.GetHashCode();
                result = (result*397) ^ (DeviceDescriptor != null ? DeviceDescriptor.GetHashCode() : 0);
                return result;
            }
        }

        public static bool operator ==(LinuxDevItem left, LinuxDevItem right) { return Equals(left, right); }
        public static bool operator !=(LinuxDevItem left, LinuxDevItem right) { return !Equals(left, right); }
    }
}