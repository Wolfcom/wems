
namespace USBKitcs.Main
{
    /// <summary> Various USB constants.
    /// </summary> 
    public static class UsbConstants
    {
        /// <summary>
        /// Default timeout for all USB IO operations.
        /// </summary>
        public const int DEFAULT_TIMEOUT = 1000;

        internal const bool EXIT_CONTEXT = false;

        /// <summary>
        /// Maximum size of a config descriptor.
        /// </summary>
        public const int MAX_CONFIG_SIZE = 4096;

        /// <summary>
        /// Maximum number of USB devices.
        /// </summary>
        public const int MAX_DEVICES = 128;

        /// <summary>
        /// Maximum number of endpoints per device.
        /// </summary>
        public const int MAX_ENDPOINTS = 32;

        /// <summary>
        /// Endpoint direction mask.
        /// </summary>
        public const byte ENDPOINT_DIR_MASK = 0x80;

        /// <summary>
        /// Endpoint number mask.
        /// </summary>
        public const byte ENDPOINT_NUMBER_MASK = 0xf;

        ///// <summary>
        ///// See <see cref="UsbError.Handled"/>.  Number of RETRIES before failed regardless of the handled field value.
        ///// </summary>
        //public const int MAX_FAIL_RETRIES_ON_HANDLED_ERROR = 4;

    }
}