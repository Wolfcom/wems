#ifndef SDK_EXPORTS_H_INCLUDED
#define SDK_EXPORTS_H_INCLUDED

#ifdef _WIN32
    #ifdef sdk_EXPORTS
        #define  SDK_EXPORT __declspec( dllexport )
    #else
        #define  SDK_EXPORT __declspec( dllimport )
    #endif
#else
    #define    SDK_EXPORT
#endif

#endif // SDK_EXPORTS_H_INCLUDED