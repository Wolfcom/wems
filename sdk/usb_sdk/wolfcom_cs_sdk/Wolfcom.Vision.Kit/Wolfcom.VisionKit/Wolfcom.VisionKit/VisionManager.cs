﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using USBKitcs;
using USBKitcs.DeviceNotify;
using USBKitcs.Main;


namespace Wolfcom.VisionKit
{
    public class VisionManager
    {
        private IDeviceNotifier _deviceNotifier;
        private IUsbDeviceEventDetector _usbNotifier = new UsbDeviceEventDetector();
        private List<VisionDevice> _devices;

        public VisionDevice[] Devices
        {
            get
            {
                return this._devices.ToArray();
            }
        }

        public event EventHandler DeviceInserted;

        public event EventHandler<DeviceMountedEventArgs> DeviceMounted;
        
        public event EventHandler<DeviceMountedEventArgs> DeviceDismounted;

        public event EventHandler DeviceRemoved;

        public VisionManager()
        {
            this._devices = new List<VisionDevice>();
            this.InitNotifiers();
        }

        private void InitNotifiers()
        {
            this._usbNotifier.DriveInserted += (EventHandler<DriveChangedEventArgs>)((s, e) => this.DeviceMountNotified(e.Drive));
            this._usbNotifier.DriveRemoved += (EventHandler<DriveChangedEventArgs>)((s, e) => this.DeviceDismountNotified(e.Drive));
            this._deviceNotifier = DeviceNotifier.OpenDeviceNotifier();
            this._deviceNotifier.OnDeviceNotify += new EventHandler<DeviceNotifyEventArgs>(this.DeviceNotified);
        }

        private void DeviceNotified(object sender, DeviceNotifyEventArgs e)
        {
            switch (e.EventType)
            {
                case EventType.DeviceArrival:
                    string[] deviceLocations = this._devices.Select<VisionDevice, string>((Func<VisionDevice, string>)(x => x.Location)).ToArray<string>();
                    Array.ForEach<UsbVisionDevice>(UsbDevice.AllLibUsbDevices.Where<UsbRegistry>((Func<UsbRegistry, bool>)(x => !((IEnumerable<string>)deviceLocations).Contains<string>(x.DeviceProperties["LocationInformation"].ToString()))).Select<UsbRegistry, UsbVisionDevice>((Func<UsbRegistry, UsbVisionDevice>)(x => new UsbVisionDevice(x))).ToArray<UsbVisionDevice>(), new Action<UsbVisionDevice>(this.CreateDevice));
                    break;
                case EventType.DeviceRemoveComplete:
                    string[] usbLocations = UsbDevice.AllLibUsbDevices.Select<UsbRegistry, string>((Func<UsbRegistry, string>)(x => x.DeviceProperties["LocationInformation"].ToString())).ToArray<string>();
                    Array.ForEach<VisionDevice>(this._devices.Where<VisionDevice>((Func<VisionDevice, bool>)(x => !((IEnumerable<string>)usbLocations).Contains<string>(x.Location))).ToArray<VisionDevice>(), new Action<VisionDevice>(this.RemoveDevice));
                    break;
            }
        }

        private void DeviceMountNotified(string drive)
        {
            if (this.DeviceMounted == null || !Directory.Exists(drive))
                return;
            this.DeviceMounted((object)this, new DeviceMountedEventArgs(drive));
        }

        private void DeviceDismountNotified(string drive)
        {
            if (this.DeviceDismounted == null)
                return;
            this.DeviceDismounted((object)this, new DeviceMountedEventArgs(drive));
        }

        private void RemoveDevices()
        {
            ((IEnumerable<VisionDevice>)this.Devices).Where<VisionDevice>((Func<VisionDevice, bool>)(dev => dev.IsDeviceInserted())).ToList<VisionDevice>().ForEach(new Action<VisionDevice>(this.RemoveDevice));
        }

        private void RemoveDevice(VisionDevice device)
        {
            device.Dispose();
            this._devices.Remove(device);
            if (this.DeviceRemoved != null)
                this.DeviceRemoved((object)device, EventArgs.Empty);
            device.FireRemoved();
        }

        public void ScanDevices()
        {
            Array.ForEach<VisionDevice>(this.Devices, new Action<VisionDevice>(this.RemoveDevice));
            Array.ForEach<UsbVisionDevice>(UsbVisionDevice.GetVisionDevices(), new Action<UsbVisionDevice>(this.CreateDevice));
        }

        private void CreateDevice(UsbVisionDevice dev)
        {
            VisionDevice visionDevice = new VisionDevice(dev);
            this._devices.Add(visionDevice);
            if (this.DeviceInserted == null)
                return;
            this.DeviceInserted((object)visionDevice, EventArgs.Empty);
        }
    }
}
