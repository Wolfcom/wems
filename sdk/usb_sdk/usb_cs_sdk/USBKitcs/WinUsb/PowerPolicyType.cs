
namespace USBKitcs.WinUsb
{
    internal enum PowerPolicyType : byte
    {
        AutoSuspend = 0x81,
        EnableWake = 0x82,
        SuspendDelay = 0x83,
    }
}