
using System;
using System.Runtime.InteropServices;

namespace USBKitcs.Main
{
    /// <summary>
    /// Base class for all critial handles.
    /// </summary>
    public abstract class SafeContextHandle : SafeHandle
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pHandle"></param>
        /// <param name="ownsHandle"></param>
        protected SafeContextHandle(IntPtr pHandle, bool ownsHandle)
            : base(IntPtr.Zero, ownsHandle) { SetHandle(pHandle); }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pHandleToOwn"></param>
        protected SafeContextHandle(IntPtr pHandleToOwn)
            : this(pHandleToOwn, true) { }

        /// <summary>
        /// Gets a value indicating whether the handle value is invalid.
        /// </summary>
        /// <returns>
        /// true if the handle value is invalid; otherwise, false.
        /// </returns>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode"/></PermissionSet>
        public override bool IsInvalid
        {
            get
            {
                if (handle != IntPtr.Zero)
                {
                    return (handle == new IntPtr(-1));
                }
                return true;
            }
        }
    }
}