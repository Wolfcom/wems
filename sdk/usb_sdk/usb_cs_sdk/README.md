# USBKitcs and WinUSB (Dependency) and LibUSB (Linux and Driver Filtering)

WinUSB is a user mode API available for Windows (7, 8/8.1, 10), allowing low level access to USB devices such as control transfers and reading from and writing to endpoints.


## Features

  * CLS compliant library (usable from all .NET languages such as C#, C++/CLI .NET, C++/CX .NET, F# and C++ * (LibUSB )
  * Synchronous and asynchronous data transfers
  * Support for 64-bit Windows versions
  * Notification events for device attachment and removal
  * Support for multiple interfaces and endpoints
  * Intellisense documentation


