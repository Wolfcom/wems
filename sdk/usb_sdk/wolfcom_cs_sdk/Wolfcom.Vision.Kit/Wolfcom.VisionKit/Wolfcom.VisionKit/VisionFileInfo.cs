﻿using System.Runtime.InteropServices;

namespace Wolfcom.VisionKit
{
    internal struct VisionFileInfo
    {
        
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        internal byte[] stat;
        internal int fs_type;
        internal ulong fstfz;
        internal ushort fstact;
        internal ushort fstad;
        internal byte fstautc;
        internal ushort fstut;
        internal ushort fstuc;
        internal ushort fstud;
        internal byte fstuutc;
        internal ushort fstct;
        internal ushort fstcd;
        internal ushort fstcc;
        internal byte fstcutc;
        internal ushort fstat;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 13)]
        internal byte[] filename;
    }
}

