

using System;

namespace USBKitcs.Main
{
    /// <summary>
    /// Standard Device Requests.
    /// </summary>
    [Flags]
    public enum UsbStandardRequest : byte
    {
        /// <summary>
        /// Clear or disable a specific feature.
        /// </summary>
        ClearFeature = 0x01,
        /// <summary>
        /// Returns the current device Configuration value.
        /// </summary>
        GetConfiguration = 0x08,
        /// <summary>
        /// Returns the specified descriptor if the descriptor exists.
        /// </summary>
        GetDescriptor = 0x06,
        /// <summary>
        /// Returns the selected alternate setting for the specified interface.
        /// </summary>
        GetInterface = 0x0A,
        /// <summary>
        /// Returns status for the specified recipient.
        /// </summary>
        GetStatus = 0x00,
        /// <summary>
        /// Sets the device address for all future device accesses.
        /// </summary>
        SetAddress = 0x05,
        /// <summary>
        /// Sets the device Configuration.
        /// </summary>
        SetConfiguration = 0x09,
        /// <summary>
        /// Optional and may be used to update existing descriptors or new descriptors may be added.
        /// </summary>
        SetDescriptor = 0x07,
        /// <summary>
        /// used to set or enable a specific feature.
        /// </summary>
        SetFeature = 0x03,
        /// <summary>
        /// Allows the host to select an alternate setting for the specified interface.
        /// </summary>
        SetInterface = 0x0B,
        /// <summary>
        /// Used to set and then report an endpointís synchronization frame.
        /// </summary>
        SynchFrame = 0x0C,
    }
}