
using System.IO;

namespace USBKitcs.DeviceNotify.Linux
{
    ///<summary>
    /// Modes the linux device notifier can use to detect notification events.
    ///</summary>
    public enum LinuxDeviceNotifierMode
    {
        /// <summary>
        /// The device notifier is unavailable on this platform.
        /// </summary>
        None,
        /// <summary>
        /// The device notifier is polling the device list every 750ms to detect usb add and removal events.
        /// </summary>
        PollDeviceList,
        /// <summary>
        /// The device notifier is using a <see cref="FileSystemWatcher"/> to monitor the "/dev" directory for file add and delete.
        /// </summary>
        MonitorDevDirectory
    }
}