

namespace USBKitcs.MonoLibUsb.Transfer
{
    /// <summary>
    /// Transfer status codes. 
    /// </summary>
    public enum MonoUsbTansferStatus
    {
        /// <summary>
        /// Transfer completed without error. Note that this does not indicate that the entire amount of requested data was transferred.
        /// </summary>
        TransferCompleted,

        /// <summary>
        /// Transfer failed 
        /// </summary>
        TransferError,

        /// <summary>
        /// Transfer timed out 
        /// </summary>
        TransferTimedOut,

        /// <summary>
        /// Transfer was cancelled 
        /// </summary>
        TransferCancelled,

        /// <summary>
        /// For bulk/interrupt endpoints: halt condition detected (endpoint stalled). For control endpoints: control request not supported. 
        /// </summary>
        TransferStall,

        /// <summary>
        /// Device was disconnected 
        /// </summary>
        TransferNoDevice,

        /// <summary>
        /// Device sent more data than requested 
        /// </summary>
        TransferOverflow
    } ;
}