
using System;

namespace USBKitcs.Main
{
    /// <summary> Endpoint data received arguments returned by the <see cref="E:LibUsbDotNet.UsbEndpointReader.DataReceived"/> event.
    /// </summary> 
    public class EndpointDataEventArgs : EventArgs
    {
        private readonly byte[] mBytesReceived;
        private readonly int mCount;

        internal EndpointDataEventArgs(byte[] bytes, int size)
        {
            mBytesReceived = bytes;
            mCount = size;
        }

        /// <summary>
        /// Gets the buffer of the received data.
        /// </summary>
        /// <remarks>
        /// Use the <see cref="EndpointDataEventArgs.Count"/> property to determine the number of bytes actually received.
        /// </remarks>
        public byte[] Buffer
        {
            get { return mBytesReceived; }
        }

        /// <summary>
        /// Gets the number of bytes received.
        /// </summary>
        public int Count
        {
            get { return mCount; }
        }
    }
}