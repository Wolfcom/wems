 using System;
using USBKitcs.Descriptors;
using USBKitcs.Main;
using USBKitcs.MonoLibUsb.Descriptors;

namespace USBKitcs.DeviceInfo
{
    /// <summary> Contains Endpoint information for the current <see cref="T:LibUsbDotNet.Info.UsbConfigInfo"/>.
    /// </summary> 
    public class UsbEndpointInfo : UsbBaseInfo
    {
        internal UsbEndpointDescriptor mUsbEndpointDescriptor;

        internal UsbEndpointInfo(byte[] descriptor)
        {
            mUsbEndpointDescriptor = new UsbEndpointDescriptor();
            SysOps.BytesToObject(descriptor, 0, Math.Min(UsbEndpointDescriptor.Size, descriptor[0]), mUsbEndpointDescriptor);
        }

        internal UsbEndpointInfo(MonoUsbEndpointDescriptor monoUsbEndpointDescriptor) { mUsbEndpointDescriptor = new UsbEndpointDescriptor(monoUsbEndpointDescriptor); }

        /// <summary>
        /// Gets the <see cref="UsbEndpointDescriptor"/> information.
        /// </summary>
        public UsbEndpointDescriptor Descriptor
        {
            get { return mUsbEndpointDescriptor; }
        }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="UsbEndpointInfo"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="UsbEndpointInfo"/>.
        ///</returns>
        public override string ToString() { return Descriptor.ToString(); }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="UsbEndpointInfo"/>.
        ///</summary>
        ///
        ///<param name="prefixSeperator">The field prefix string.</param>
        ///<param name="entitySperator">The field/value seperator string.</param>
        ///<param name="suffixSeperator">The value suffix string.</param>
        ///<returns>A formatted representation of the <see cref="UsbEndpointInfo"/>.</returns>
        public string ToString(string prefixSeperator, string entitySperator, string suffixSeperator) { return Descriptor.ToString(prefixSeperator, entitySperator, suffixSeperator); }
    }
}