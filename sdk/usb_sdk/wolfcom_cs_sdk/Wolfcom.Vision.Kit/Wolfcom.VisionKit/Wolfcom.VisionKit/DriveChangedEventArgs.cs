﻿using System;

namespace Wolfcom.VisionKit
{
    internal class DriveChangedEventArgs : EventArgs
    {
        internal readonly string Drive;

        internal DriveChangedEventArgs(string drive)
        {
            this.Drive = drive;
        }
    }
}
