
using System;
using System.Runtime.InteropServices;
using USBKitcs.Internal;
using USBKitcs.Main;

namespace USBKitcs
{
    /// <summary>Contains methods for writing data to a <see cref="EndpointType.Bulk"/> or <see cref="EndpointType.Interrupt"/> endpoint using the overloaded <see cref="Write(byte[],int,out int)"/> functions.
    /// </summary> 
    public class UsbEndpointWriter : UsbEndpointBase
    {
        internal UsbEndpointWriter(UsbDevice usbDevice, WriteEndpointID writeEndpointID, EndpointType endpointType)
            : base(usbDevice, (byte)writeEndpointID, endpointType) { }


        /// <summary>
        /// Writes data to the current <see cref="UsbEndpointWriter"/>.
        /// </summary>
        /// <param name="buffer">The buffer storing the data to write.</param>
        /// <param name="timeout">Maximum time to wait for the transfer to complete.  If the transfer times out, the IO operation will be cancelled.</param>
        /// <param name="transferLength">Number of bytes actually transferred.</param>
        /// <returns>
        /// <see cref="ErrorCode"/>.<see cref="ErrorCode.None"/> on success.
        /// </returns>
        public virtual ErrorCode Write(byte[] buffer, int timeout, out int transferLength) { return Write(buffer, 0, buffer.Length, timeout, out transferLength); }

        /// <summary>
        /// Writes data to the current <see cref="UsbEndpointWriter"/>.
        /// </summary>
        /// <param name="pBuffer">The buffer storing the data to write.</param>
        /// <param name="offset">The position in buffer to start writing the data from.</param>
        /// <param name="count">The number of bytes to write.</param>
        /// <param name="timeout">Maximum time to wait for the transfer to complete.  If the transfer times out, the IO operation will be cancelled.</param>
        /// <param name="transferLength">Number of bytes actually transferred.</param>
        /// <returns>
        /// <see cref="ErrorCode"/>.<see cref="ErrorCode.None"/> on success.
        /// </returns>
        public virtual ErrorCode Write(IntPtr pBuffer, int offset, int count, int timeout, out int transferLength) { return Transfer(pBuffer, offset, count, timeout, out transferLength); }

        /// <summary>
        /// Writes data to the current <see cref="UsbEndpointWriter"/>.
        /// </summary>
        /// <param name="buffer">The buffer storing the data to write.</param>
        /// <param name="offset">The position in buffer to start writing the data from.</param>
        /// <param name="count">The number of bytes to write.</param>
        /// <param name="timeout">Maximum time to wait for the transfer to complete.  If the transfer times out, the IO operation will be cancelled.</param>
        /// <param name="transferLength">Number of bytes actually transferred.</param>
        /// <returns>
        /// <see cref="ErrorCode"/>.<see cref="ErrorCode.None"/> on success.
        /// </returns>
        public virtual ErrorCode Write(byte[] buffer, int offset, int count, int timeout, out int transferLength) { return Transfer(buffer, offset, count, timeout, out transferLength); }

        /// <summary>
        /// Writes data to the current <see cref="UsbEndpointWriter"/>.
        /// </summary>
        /// <param name="buffer">The buffer storing the data to write.</param>
        /// <param name="offset">The position in buffer to start writing the data from.</param>
        /// <param name="count">The number of bytes to write.</param>
        /// <param name="timeout">Maximum time to wait for the transfer to complete.  If the transfer times out, the IO operation will be cancelled.</param>
        /// <param name="transferLength">Number of bytes actually transferred.</param>
        /// <returns>
        /// <see cref="ErrorCode"/>.<see cref="ErrorCode.None"/> on success.
        /// </returns>
        public virtual ErrorCode Write(object buffer, int offset, int count, int timeout, out int transferLength) { return Transfer(buffer, offset, count, timeout, out transferLength); }

        /// <summary>
        /// Writes data to the current <see cref="UsbEndpointWriter"/>.
        /// </summary>
        /// <param name="buffer">The buffer storing the data to write.</param>
        /// <param name="timeout">Maximum time to wait for the transfer to complete.  If the transfer times out, the IO operation will be cancelled.</param>
        /// <param name="transferLength">Number of bytes actually transferred.</param>
        /// <returns>
        /// <see cref="ErrorCode"/>.<see cref="ErrorCode.None"/> on success.
        /// </returns>
        public virtual ErrorCode Write(object buffer, int timeout, out int transferLength) { return Write(buffer, 0, Marshal.SizeOf(buffer), timeout, out transferLength); }

        internal override UsbTransfer CreateTransferContext() { return new OverlappedTransferContext(this); }
    }
}