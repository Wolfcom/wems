using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace USBKitcs.DeviceInfo
{
    /// <summary> Base class for all Usb descriptors.
    ///         <see cref="USBKitcs.DeviceInfo.UsbConfigInfo"/>, <see cref="T:USBKitcs.DeviceInfo.UsbEndpointInfo"/>, <see cref="T:USBKitcs.Info.UsbInterfaceInfo"/></summary>
    /// <remarks><p>USBKitcs supports and parses all the basic usb descriptors.</p><p>
    ///           Unknown descriptors such as driver specific class descriptors are stored as byte arrays and are accessible from the <see cref="P:USBKitcs.DeviceInfo.UsbBaseInfo.CustomDescriptors"/> property.
    ///         </p></remarks>
    public abstract class UsbBaseInfo
    {
        internal List<byte[]> mRawDescriptors = new List<byte[]>();

        /// <summary>
        /// Gets the device-specific custom descriptor lists.
        /// </summary>
        public ReadOnlyCollection<byte[]> CustomDescriptors
        {
            get { return mRawDescriptors.AsReadOnly(); }
        }
    }
}