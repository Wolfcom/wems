
using System;

namespace USBKitcs.Main
{
    /// <summary> All possible USB endpoint types.
    /// </summary> 
    [Flags]
    public enum EndpointType : byte
    {
        /// <summary>
        /// Control endpoint type.
        /// </summary>
        Control,
        /// <summary>
        /// Isochronous endpoint type.
        /// </summary>
        Isochronous,
        /// <summary>
        /// Bulk endpoint type.
        /// </summary>
        Bulk,
        /// <summary>
        /// Interrupt endpoint type.
        /// </summary>
        Interrupt
    }
}