﻿namespace Wolfcom.VisionKit
{
    public enum PhotoResolutions
    {
        Resolution16M,
        Resolution12M,
        Resolution8M,
        Resolution5M,
        Resolution3M,
    }
}
