﻿using System;
using System.Text;

namespace Wolfcom.ThirdEyeKit
{
    public class ThirdEyeFile
    {
        private ThirdEyeFileInfo _fi;
        private int _index;

        public string Name
        {
            get
            {
                return Encoding.ASCII.GetString(this._fi.filename).TrimEnd(char.MinValue, ' ');
            }
        }

        public ulong Length
        {
            get
            {
                return this._fi.fstfz;
            }
        }

        public DateTime LastAccessTime
        {
            get
            {
                return this.GetTime(this._fi.fstad, this._fi.fstact, this._fi.fstautc);
            }
        }

        public DateTime LastUpdateTime
        {
            get
            {
                return this.GetTime(this._fi.fstud, this._fi.fstuc, this._fi.fstuutc);
            }
        }

        public DateTime CreationTime
        {
            get
            {
                return this.GetTime(this._fi.fstcd, this._fi.fstcc, this._fi.fstcutc);
            }
        }

        public ThirdEyeDevice Device { get; private set; }

        public event EventHandler<ThirdEyeFileImportEventArgs> ImportProgress;

        internal ThirdEyeFile(ThirdEyeDevice device, ThirdEyeFileInfo fi, int index)
        {
            this._fi = fi;
            this.Device = device;
            this._index = index;
        }

        public void ImportFile(string destination)
        {
            this.Device._dev.ImportFile(destination, this._index, this.Length, this.ImportProgress);
        }

        public override string ToString()
        {
            return string.Format("{0} - ({1}KB)", (object)this.Name, (object)(this.Length / 1024UL));
        }

        private DateTime GetTime(ushort date, ushort time, byte utcOffset)
        {
            return new DateTime(((int)date >> 9 & (int)sbyte.MaxValue) + 1980, (int)date >> 5 & 15, (int)date & 31, (int)time >> 11 & 31, (int)time >> 5 & 63, ((int)time & 31) * 2);
        }
    }
}

