
using System;
using System.Runtime.InteropServices;

using USBKitcs.Main;

namespace USBKitcs.MonoLibUsb.Transfer.Internal
{
    /// <remarks>
    /// This class is never instantiated in .NET.  Instead it is used as a template by the <see cref="MonoUsbTransfer"/> class.
    /// </remarks>
    [StructLayout(LayoutKind.Sequential, Pack = MonoUsbApi.LIBUSB_PACK)]
    internal class libusb_transfer
    {
        IntPtr deviceHandle;
        MonoUsbTransferFlags flags;
        byte endpoint;
        EndpointType type;
        uint timeout;
        MonoUsbTansferStatus status;
        int length;
        int actual_length;
        IntPtr pCallbackFn;
        IntPtr pUserData;
        IntPtr pBuffer;
        int num_iso_packets;
        IntPtr iso_packets;

        private libusb_transfer() { }

    } ;
}