
namespace USBKitcs.WinUsb
{
    /// <summary> Types of information that can be retrieved with the WinUsb QueryDevice function.
    /// </summary> 
    public enum DeviceInformationTypes : byte
    {
        /// <summary>
        /// The device speed.
        /// </summary>
        DeviceSpeed = 0x01
    }
}