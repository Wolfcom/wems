using System;
using USBKitcs.DeviceNotify.Info;
using USBKitcs.DeviceNotify.Internal;

namespace USBKitcs.DeviceNotify
{
    /// <summary>
    /// Describes the device notify event
    /// </summary> 
    public class WindowsDeviceNotifyEventArgs : DeviceNotifyEventArgs
    {
        private readonly DevBroadcastHdr mBaseHdr;

        internal WindowsDeviceNotifyEventArgs(DevBroadcastHdr hdr, IntPtr ptrHdr, EventType eventType)
        {
            mBaseHdr = hdr;
            mEventType = eventType;
            mDeviceType = mBaseHdr.DeviceType;
            switch (mDeviceType)
            {
                case DeviceType.Volume:
                    mVolume = new VolumeNotifyInfo(ptrHdr);
                    mObject = mVolume;
                    break;
                case DeviceType.Port:
                    mPort = new PortNotifyInfo(ptrHdr);
                    mObject = mPort;
                    break;
                case DeviceType.DeviceInterface:
                    mDevice = new UsbDeviceNotifyInfo(ptrHdr);
                    mObject = mDevice;
                    break;
            }
        }
    }
}