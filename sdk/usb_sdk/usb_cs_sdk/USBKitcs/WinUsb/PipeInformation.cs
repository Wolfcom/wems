
using System.Runtime.InteropServices;
using USBKitcs.Main;

namespace USBKitcs.WinUsb
{
    /// <summary> WinUsb Pipe information.
    /// </summary> 
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PipeInformation
    {
        /// <summary>
        /// Size of the structure in bytes.
        /// </summary>
        public static readonly int Size = Marshal.SizeOf(typeof (PipeInformation));

        /// <summary>
        /// Specifies the pipe type.
        /// </summary>
        public EndpointType PipeType;

        /// <summary>
        /// The pipe identifier (ID). 
        /// </summary>
        public byte PipeId;

        /// <summary>
        /// The maximum size, in bytes, of the packets that are transmitted on the pipe.
        /// </summary>
        public short MaximumPacketSize;

        /// <summary>
        /// The pipe interval.
        /// </summary>
        public byte Interval;
    }
}