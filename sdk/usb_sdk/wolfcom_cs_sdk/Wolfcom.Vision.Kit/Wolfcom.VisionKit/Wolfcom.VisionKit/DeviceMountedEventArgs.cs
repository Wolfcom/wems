﻿using System;

namespace Wolfcom.VisionKit
{
    public class DeviceMountedEventArgs : EventArgs
    {
        public string Drive { get; private set; }

        internal DeviceMountedEventArgs(string drive)
        {
            this.Drive = drive;
        }
    }
}
