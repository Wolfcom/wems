
using System;
using System.Text;
using USBKitcs.Internal.LibUsb;
using USBKitcs.Main;

namespace USBKitcs.LibUsb
{
    internal static class LibUsbDeviceRegistryKeyRequest
    {
        public static byte[] RegGetRequest(string name, int valueBufferSize)
        {
            if (valueBufferSize < 1 || name.Trim().Length == 0) throw new UsbException("Global", "Invalid DeviceRegistry het parameter.");

            LibUsbRequest req = new LibUsbRequest();
            int uOffset = LibUsbRequest.Size;
            req.DeviceRegKey.KeyType = (int) KeyType.RegBinary;

            byte[] bytesName = Encoding.Unicode.GetBytes(name + "\0");

            req.DeviceRegKey.NameOffset = uOffset;
            uOffset += bytesName.Length;
            req.DeviceRegKey.ValueOffset = uOffset;
            req.DeviceRegKey.ValueLength = (valueBufferSize);

            uOffset += Math.Max(uOffset + 1, valueBufferSize - (LibUsbRequest.Size + bytesName.Length));
            byte[] buffer = new byte[uOffset];
            byte[] regBytes = req.Bytes;

            Array.Copy(regBytes, buffer, regBytes.Length);
            Array.Copy(bytesName, 0, buffer, req.DeviceRegKey.NameOffset, bytesName.Length);

            return buffer;
        }

        public static byte[] RegSetBinaryRequest(string name, byte[] value)
        {
            LibUsbRequest req = new LibUsbRequest();
            int uOffset = LibUsbRequest.Size;
            req.DeviceRegKey.KeyType = (int) KeyType.RegBinary;

            byte[] bytesName = Encoding.Unicode.GetBytes(name + "\0");
            byte[] bytesValue = value;

            req.DeviceRegKey.NameOffset = uOffset;
            uOffset += bytesName.Length;
            req.DeviceRegKey.ValueOffset = uOffset;
            req.DeviceRegKey.ValueLength = bytesValue.Length;

            uOffset += bytesValue.Length;
            byte[] buffer = new byte[uOffset];
            byte[] regBytes = req.Bytes;

            Array.Copy(regBytes, buffer, regBytes.Length);
            Array.Copy(bytesName, 0, buffer, req.DeviceRegKey.NameOffset, bytesName.Length);
            Array.Copy(bytesValue, 0, buffer, req.DeviceRegKey.ValueOffset, bytesValue.Length);

            return buffer;
        }

        public static byte[] RegSetStringRequest(string name, string value)
        {
            LibUsbRequest req = new LibUsbRequest();
            int uOffset = LibUsbRequest.Size;
            req.DeviceRegKey.KeyType = (int) KeyType.RegSz;

            byte[] bytesName = Encoding.Unicode.GetBytes(name + "\0");
            byte[] bytesValue = Encoding.Unicode.GetBytes(value + "\0");

            req.DeviceRegKey.NameOffset = uOffset;
            uOffset += bytesName.Length;
            req.DeviceRegKey.ValueOffset = uOffset;
            req.DeviceRegKey.ValueLength = bytesValue.Length;

            uOffset += bytesValue.Length;
            byte[] buffer = new byte[uOffset];
            byte[] regBytes = req.Bytes;

            Array.Copy(regBytes, buffer, regBytes.Length);
            Array.Copy(bytesName, 0, buffer, req.DeviceRegKey.NameOffset, bytesName.Length);
            Array.Copy(bytesValue, 0, buffer, req.DeviceRegKey.ValueOffset, bytesValue.Length);

            return buffer;
        }

        #region Nested Types

        private enum KeyType
        {
            RegSz = 1, // Unicode nul terminated string
            RegBinary = 3, // Free form binary
        }

        #endregion
    }
}