using System;
using System.Windows.Forms;

namespace USBKitcs.DeviceNotify.Internal
{
    internal sealed class DevNotifyNativeWindow : NativeWindow
    {
        private const string WINDOW_CAPTION = "{18662f14-0871-455c-bf99-eff135425e3a}";
        private const int WM_DEVICECHANGE = 0x219;
        private readonly OnDeviceChangeDelegate mDelDeviceChange;
        private readonly OnHandleChangeDelegate mDelHandleChanged;

        internal DevNotifyNativeWindow(OnHandleChangeDelegate delHandleChanged, OnDeviceChangeDelegate delDeviceChange)
        {
            mDelHandleChanged = delHandleChanged;
            mDelDeviceChange = delDeviceChange;

            CreateParams cp = new CreateParams();
            cp.Caption = WINDOW_CAPTION;
            cp.X = -100;
            cp.Y = -100;
            cp.Width = 50;
            cp.Height = 50;
            CreateHandle(cp);
        }

        protected override void OnHandleChange()
        {
            mDelHandleChanged(Handle);
            base.OnHandleChange();
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_DEVICECHANGE)
            {
                mDelDeviceChange(ref m);
            }
            base.WndProc(ref m);
        }

        #region Nested Types

        #region Nested type: OnDeviceChangeDelegate

        internal delegate void OnDeviceChangeDelegate(ref Message m);

        #endregion

        #region Nested type: OnHandleChangeDelegate

        internal delegate void OnHandleChangeDelegate(IntPtr windowHandle);

        #endregion

        #endregion
    }
}