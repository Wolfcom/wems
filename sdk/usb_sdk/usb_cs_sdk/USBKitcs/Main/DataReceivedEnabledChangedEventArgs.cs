using System;

namespace USBKitcs.Main
{
    /// <summary>
    /// Event arguments that are passed when <see cref="UsbEndpointReader.DataReceivedEnabled"/> is changes state.
    /// </summary>
    public class DataReceivedEnabledChangedEventArgs : EventArgs
    {
        private readonly bool mEnabled;
        private readonly ErrorCode mErrorCode = ErrorCode.Success;

        internal DataReceivedEnabledChangedEventArgs(bool enabled, ErrorCode errorCode)
        {
            mEnabled = enabled;
            mErrorCode = errorCode;
        }

        internal DataReceivedEnabledChangedEventArgs(bool enabled)
            : this(enabled, ErrorCode.Success) { }

        /// <summary>
        /// The <see cref="Main.ErrorCode"/> that caused the <see cref="UsbEndpointReader.DataReceived"/> event to terminate.
        /// </summary>
        public ErrorCode ErrorCode
        {
            get { return mErrorCode; }
        }

        /// <summary>
        /// <c>True</c> if <see cref="UsbEndpointReader.DataReceivedEnabled"/> changes from <c>True</c> to <c>False</c>. 
        /// </summary>
        public bool Enabled
        {
            get { return mEnabled; }
        }
    }
}