using System.Runtime.InteropServices;

#pragma warning disable 169

namespace USBKitcs.DeviceNotify.Internal
{
    [StructLayout(LayoutKind.Sequential)]
    internal class DevBroadcastPort : DevBroadcastHdr
    {
        private char mNameHolder;

        public DevBroadcastPort()
        {
            Size = Marshal.SizeOf(this);
            DeviceType = DeviceType.Port;
        }
    }
}