
using System;
using System.Runtime.InteropServices;

namespace USBKitcs.WinUsb
{
    /// <summary> 
    /// power policy for a <see cref="WinUsbDevice"/>.
    /// </summary> 
    public class PowerPolicies
    {
        private const int MAX_SIZE = 4;
        private readonly WinUsbDevice mUsbDevice;
        private IntPtr mBufferPtr = IntPtr.Zero;

        internal PowerPolicies(WinUsbDevice usbDevice)
        {
            mBufferPtr = Marshal.AllocCoTaskMem(MAX_SIZE);
            mUsbDevice = usbDevice;
        }

        /// <summary>
        /// If the auto suspend policy parameter is TRUE (that is, nonzero), the USB stack suspends the device when no transfers are pending. The default value for the AutoSuspend policy parameter is TRUE.
        /// </summary>
        public bool AutoSuspend
        {
            get
            {
                int iValueLength = 1;
                Marshal.WriteByte(mBufferPtr, 0);
                bool bSuccess = mUsbDevice.GetPowerPolicy(PowerPolicyType.AutoSuspend, ref iValueLength, mBufferPtr);
                if (bSuccess)
                    return Marshal.ReadByte(mBufferPtr) == 0 ? false : true;
                return false;
            }
            set
            {
                int iValueLength = 1;
                byte bPowerPolicyValue = (value) ? (byte) 1 : (byte) 0;
                Marshal.WriteByte(mBufferPtr, bPowerPolicyValue);
                mUsbDevice.SetPowerPolicy(PowerPolicyType.AutoSuspend, iValueLength, mBufferPtr);
            }
        }

        /// <summary>
        /// The suspend delay policy parameter specifies the minimum amount of time, in milliseconds, that the WinUSB driver must wait after any transfer before it can suspend the device. 
        /// </summary>
        public int SuspendDelay
        {
            get
            {
                int iValueLength = Marshal.SizeOf(typeof (int));
                Marshal.WriteInt32(mBufferPtr, 0);
                bool bSuccess = mUsbDevice.GetPowerPolicy(PowerPolicyType.SuspendDelay, ref iValueLength, mBufferPtr);
                if (bSuccess)
                    return Marshal.ReadInt32(mBufferPtr);
                return -1;
            }
            set
            {
                int iValueLength = Marshal.SizeOf(typeof (int));
                Marshal.WriteInt32(mBufferPtr, value);
                mUsbDevice.SetPowerPolicy(PowerPolicyType.SuspendDelay, iValueLength, mBufferPtr);
            }
        }

        /// <summary>
        /// Frees instance resources.
        /// </summary>
        ~PowerPolicies()
        {
            if (mBufferPtr != IntPtr.Zero)
                Marshal.FreeCoTaskMem(mBufferPtr);

            mBufferPtr = IntPtr.Zero;
        }
    }
}