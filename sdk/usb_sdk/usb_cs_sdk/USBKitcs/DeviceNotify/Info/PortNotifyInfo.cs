
using System;
using System.Runtime.InteropServices;
using USBKitcs.DeviceNotify.Internal;

namespace USBKitcs.DeviceNotify.Info
{
    /// <summary> Notify information for a communication port
    /// </summary> 
    public class PortNotifyInfo : IPortNotifyInfo
    {
        private readonly DevBroadcastPort mBaseHdr = new DevBroadcastPort();
        private readonly string mName;

        internal PortNotifyInfo(IntPtr lParam)
        {
            Marshal.PtrToStructure(lParam, mBaseHdr);
            IntPtr pName = new IntPtr(lParam.ToInt64() + Marshal.OffsetOf(typeof (DevBroadcastPort), "mNameHolder").ToInt64());
            mName = Marshal.PtrToStringAuto(pName);
        }

        #region IPortNotifyInfo Members

        /// <summary>
        /// Gets the name of the port that caused the event.
        /// </summary>
        public string Name
        {
            get { return mName; }
        }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="PortNotifyInfo"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="PortNotifyInfo"/>.
        ///</returns>
        public override string ToString() { return string.Format("[Port Name:{0}] ", Name); }

        #endregion
    }
}