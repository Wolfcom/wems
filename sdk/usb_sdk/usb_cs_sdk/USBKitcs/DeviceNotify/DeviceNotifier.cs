using USBKitcs.DeviceNotify.Linux;

namespace USBKitcs.DeviceNotify
{
    /// <summary>
    /// Platform independent class for linux/windows device notification.
    /// </summary>
    /// <code source="..\Examples\Device.Notification\DeviceNotification.cs" lang="cs"/>
    public static class DeviceNotifier
    {
        /// <summary>
        /// Creates a new instance of a device notifier class.
        /// </summary>
        /// <returns>A <see cref="WindowsDeviceNotifier"/> under windows and a <see cref="LinuxDeviceNotifier"/> under linux.</returns>
        public static IDeviceNotifier OpenDeviceNotifier()
        {
            if (UsbDevice.IsLinux)
            {
                return new LinuxDeviceNotifier();
            }
            else
            {
                return new WindowsDeviceNotifier();
            }
        }
    }
}