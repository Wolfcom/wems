﻿using System;

namespace Wolfcom.ThirdEyeKit
{
    internal interface IUsbDeviceEventDetector
    {
        event EventHandler<EventArgs> DeviceInsertedOrRemoved;

        event EventHandler<DriveChangedEventArgs> DriveInserted;

        event EventHandler<DriveChangedEventArgs> DriveRemoved;
    }
}
