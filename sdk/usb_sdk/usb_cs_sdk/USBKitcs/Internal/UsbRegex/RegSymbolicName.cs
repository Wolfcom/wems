
namespace USBKitcs.Internal.UsbRegex
{
    /// <summary>
    /// Regular expression class for parsing USB symbolic names and hardware ids.
    /// </summary>
    internal class RegSymbolicName : BaseRegSymbolicName
    {
        public static readonly NamedGroup[] NamedGroups = new NamedGroup[]
                                                              {
                                                                  new NamedGroup(1, "Vid"),
                                                                  new NamedGroup(2, "Pid"),
                                                                  new NamedGroup(3, "Rev"),
                                                                  new NamedGroup(4, "ClassGuid"),
                                                                  new NamedGroup(5, "String"),
                                                              };

        public new string[] GetGroupNames() { return new string[] {"Vid", "Pid", "Rev", "ClassGuid", "String"}; }

        public new int[] GetGroupNumbers() { return new int[] {1, 2, 3, 4, 5}; }

        public new string GroupNameFromNumber(int groupNumber)
        {
            switch (groupNumber)
            {
                case 1:
                    return "Vid";

                case 2:
                    return "Pid";

                case 3:
                    return "Rev";

                case 4:
                    return "ClassGuid";

                case 5:
                    return "String";
            }
            return "";
        }

        public new int GroupNumberFromName(string groupName)
        {
            switch (groupName.ToLower())
            {
                case "vid":
                    return 1;

                case "pid":
                    return 2;

                case "rev":
                    return 3;

                case "classguid":
                    return 4;

                case "string":
                    return 5;
            }
            return -1;
        }
    }

    internal enum NamedGroupType
    {
        Vid = 1,
        Pid = 2,
        Rev = 3,
        ClassGuid = 4,
        String = 5,
    }
}