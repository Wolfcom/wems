using System;
using System.Runtime.InteropServices;

using USBKitcs.Main;

#pragma warning disable 649

namespace USBKitcs.Descriptors
{
    /// <summary> Base class for all usb descriptors structures.
    /// </summary> 
    /// <remarks> This is the actual descriptor as described in the USB 2.0 Specifications.
    /// </remarks> 
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class UsbDescriptor
    {
        /// <summary>
        /// String value used to seperate the name/value pairs for all ToString overloads of the descriptor classes.
        /// </summary>
        public static string ToStringParamValueSeperator = ":";

        /// <summary>
        /// String value used to seperate the name/value groups for all ToString overloads of the descriptor classes.
        /// </summary>
        public static string ToStringFieldSeperator = "\r\n";

        /// <summary>
        /// Total size of this structure in bytes.
        /// </summary>
        public static readonly int Size = Marshal.SizeOf(typeof (UsbDescriptor));

        /// <summary>
        /// Length of structure reported by the associated usb device.
        /// </summary>
        public byte Length;

        /// <summary>
        /// Type of structure reported by the associated usb device.
        /// </summary>
        public DescriptorType DescriptorType;

        /// <summary>
        /// String representation of the UsbDescriptor class.
        /// </summary>
        public override string ToString()
        {
            Object[] values = {Length, DescriptorType};
            string[] names = {"Length", "DescriptorType"};

            return SysOps.ToString("", names, ToStringParamValueSeperator, values, ToStringFieldSeperator);
        }
    }
}