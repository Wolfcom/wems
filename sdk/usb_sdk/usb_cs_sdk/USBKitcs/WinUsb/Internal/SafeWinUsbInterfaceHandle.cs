
using System;
using System.Runtime.InteropServices;

using USBKitcs.WinUsb.Internal;

namespace USBKitcs.Internal.WinUsb
{
    internal class SafeWinUsbInterfaceHandle : SafeHandle
    {
        public SafeWinUsbInterfaceHandle()
            : base(IntPtr.Zero, true) { }

        public SafeWinUsbInterfaceHandle(IntPtr handle)
            : base(handle, true) { }

        ///<summary>
        ///Gets a value indicating whether the <see cref="SafeWinUsbInterfaceHandle"/> value is invalid.
        ///</summary>
        ///
        ///<returns>
        ///true if the <see cref="SafeWinUsbInterfaceHandle"/> is valid; otherwise, false.
        ///</returns>
        public override bool IsInvalid
        {
            get { return (handle == IntPtr.Zero || handle.ToInt64() == -1); }
        }

        ///<summary>
        ///Executes the code required to free the <see cref="SafeWinUsbInterfaceHandle"/>.
        ///</summary>
        ///
        ///<returns>
        ///true if the <see cref="SafeWinUsbInterfaceHandle"/> is released successfully; otherwise, in the event of a catastrophic failure, false. In this case, it generates a ReleaseHandleFailed Managed Debugging Assistant.
        ///</returns>
        ///
        protected override bool ReleaseHandle()
        {
            bool bSuccess = true;
            if (!IsInvalid)
            {
                bSuccess = WinUsbAPI.WinUsb_Free(handle);
                handle = IntPtr.Zero;
            }
            return bSuccess;
        }
    }
}