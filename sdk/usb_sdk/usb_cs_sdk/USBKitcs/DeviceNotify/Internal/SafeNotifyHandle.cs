using System;
using Microsoft.Win32.SafeHandles;

namespace USBKitcs.DeviceNotify.Internal
{
    internal class SafeNotifyHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        public SafeNotifyHandle()
            : base(true) { }

        public SafeNotifyHandle(IntPtr pHandle)
            : base(true) { SetHandle(pHandle); }

        protected override bool ReleaseHandle()
        {
            if (handle != IntPtr.Zero)
            {
                bool bSuccess = WindowsDeviceNotifier.UnregisterDeviceNotification(handle);
                handle = IntPtr.Zero;
            }
            return true;
        }
    }
}