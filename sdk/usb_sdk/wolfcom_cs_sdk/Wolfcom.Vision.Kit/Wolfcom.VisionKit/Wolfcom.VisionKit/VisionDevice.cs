﻿using System;
using System.Linq;

namespace Wolfcom.VisionKit
{
    public class VisionDevice : IDisposable
    {
        internal static readonly float[] TimeZoneOffsets = new float[29]
        {
      -8f,
      -7f,
      -6f,
      -5f,
      -4f,
      -3.5f,
      -3f,
      -2f,
      -1f,
      0.0f,
      1f,
      2f,
      3f,
      3.5f,
      4f,
      4.5f,
      5f,
      5.5f,
      6f,
      6.5f,
      7f,
      8f,
      9f,
      9.5f,
      10f,
      10.5f,
      11f,
      11.5f,
      12f
        };

        private const int SetupStringLength = 18;
        private const int DeviceStringLength = 64;
        internal readonly UsbVisionDevice _dev;

        public bool IsInserted
        {
            get
            {
                IntPtr handle = this.Handle;
                return true;
            }
        }

        public string PoliceId { get; set; }

        public string SerialId { get; set; }

        public string Volume { get; internal set; }

        public string Password { get; set; }

        public UserModes UserMode { get; set; }

        public VideoResolutions VideoResolution { get; set; }

        public ResolutionRates ResolutionRate { get; set; }

        public VideoFormats VideoFormat { get; set; }

        public bool AudioRecord { get; set; }

        public RecycleRecords RecycleRecord { get; set; }

        public bool PrerecordAudio { get; set; }

        public bool LoopRecord { get; set; }

        public PhotoResolutions PhotoResolution { get; set; }

        public DeviceActivityIndicators DeviceActivityIndicator { get; set; }

        public bool PasswordRequired { get; set; }

        public bool TimestampVideo { get; set; }

        public bool StampGPS { get; set; }

        public OneTouchRecordSetting OneTouchRecord { get; set; }

        public bool RequirePassword { get; set; }

        internal string Location
        {
            get
            {
                return this._dev.DeviceLocation;
            }
        }

        internal string SetupString { get; private set; }

        internal IntPtr Handle { get; set; }

        internal float TimeZoneOffset { get; private set; }

        public int FileCount
        {
            get
            {
                return this._dev.GetDeviceFileCount();
            }
        }

        public event EventHandler Removed;

        internal VisionDevice(UsbVisionDevice dev)
        {
            this._dev = dev;
            this.Removed = (EventHandler)null;
            this.SerialId = "";
            this.PoliceId = "";
            this.Volume = "";
            this.Password = "";
            this.SetupString = "";
            this.Init();
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", (object)this.SerialId, (object)this.PoliceId);
        }

        public void Mount()
        {
            this._dev.SetDeviceMassStorage();
        }

        public void Format()
        {
            char[] charArray = this._dev.GetDeviceSetup().ToCharArray();
            charArray[11] = '1';
            this._dev.SetDeviceSetup(new string(charArray));
        }

        public void Reset()
        {
            char[] array = Enumerable.Repeat<char>('0', 17).ToArray<char>();
            array[9] = '1';
            this._dev.SetDeviceSetup(new string(array));
        }

        public void SetTime(DateTime time)
        {
            this._dev.SetDeviceTime(time);
        }

        public void Commit()
        {
            this._dev.SetDeviceSetup(this.EncodeSetupString());
            this._dev.SetDevicePoliceNo(this.PoliceId);
            this._dev.SetDeviceSerialNo(this.SerialId);
            this._dev.SetDevicePassword(this.Password);
        }

        public VisionFile[] GetFiles()
        {
            return Enumerable.Range(1, this.FileCount).Select<int, VisionFile>((Func<int, VisionFile>)(i => this.GetFile(i - 1))).ToArray<VisionFile>();
        }

        public bool IsDeviceInserted()
        {
            return this._dev.IsOpen();
        }

        private VisionFile GetFile(int index)
        {
            return new VisionFile(this, this._dev.GetFileInfo(index), index);
        }

        private void Init()
        {
            this.DecodeSetupString(this._dev.GetDeviceSetup());
            this.PoliceId = this._dev.GetDevicePoliceNo();
            this.SerialId = this._dev.GetDeviceSerialNo();
            this.Password = this._dev.GetDevicePassword();
        }

        internal void FireRemoved()
        {
            this.Dispose();
            if (this.Removed == null)
                return;
            this.Removed((object)this, EventArgs.Empty);
        }

        private void DecodeSetupString(string setup)
        {
            this.PasswordRequired = (int)setup[0] == 49;
            this.VideoResolution = this.ParseEnum<VideoResolutions>(setup[1]);
            this.ResolutionRate = this.ParseEnum<ResolutionRates>(setup[2]);
            this.AudioRecord = (int)setup[3] == 48;
            this.RecycleRecord = this.ParseEnum<RecycleRecords>(setup[4]);
            this.LoopRecord = (int)setup[5] == 48;
            this.PhotoResolution = this.ParseEnum<PhotoResolutions>(setup[6]);
            this.DeviceActivityIndicator = this.ParseEnum<DeviceActivityIndicators>(setup[7]);
            this.TimestampVideo = (int)setup[8] == 49;
            this.UserMode = this.ParseEnum<UserModes>(setup[10]);
            this.VideoFormat = this.ParseEnum<VideoFormats>(setup[12]);
            this.StampGPS = (int)setup[13] == 49;
            this.PrerecordAudio = (int)setup[14] == 48;
            this.OneTouchRecord = this.ParseEnum<OneTouchRecordSetting>(setup[15]);
            this.TimeZoneOffset = VisionDevice.TimeZoneOffsets[int.Parse(setup.Substring(16, 2))];
        }

        private string EncodeSetupString()
        {
            char[] array = Enumerable.Repeat<char>('0', 18).ToArray<char>();
            array[0] = this.YesNo(this.PasswordRequired);
            array[1] = "01234"[(int)this.VideoResolution];
            array[2] = "0123"[(int)this.ResolutionRate];
            array[3] = this.NoYes(this.AudioRecord);
            array[4] = "01234"[(int)this.RecycleRecord];
            array[5] = this.NoYes(this.LoopRecord);
            array[6] = "01234"[(int)this.PhotoResolution];
            array[7] = "012"[(int)this.DeviceActivityIndicator];
            array[8] = this.YesNo(this.TimestampVideo);
            array[10] = "012"[(int)this.UserMode];
            array[12] = "01"[(int)this.VideoFormat];
            array[13] = this.YesNo(this.StampGPS);
            array[14] = this.NoYes(this.PrerecordAudio);
            array[15] = "01"[(int)this.OneTouchRecord];
            string str = Array.IndexOf<float>(VisionDevice.TimeZoneOffsets, this.TimeZoneOffset).ToString("N2");
            array[16] = str[0];
            array[17] = str[1];
            return new string(array);
        }

        private T ParseEnum<T>(char c)
        {
            return (T)Enum.Parse(typeof(T), c.ToString());
        }

        private char YesNo(bool setting)
        {
            return !setting ? '0' : '1';
        }

        private char NoYes(bool setting)
        {
            return this.YesNo(!setting);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing || !(this.Handle != IntPtr.Zero))
                return;
            this._dev.Release();
            this.Handle = IntPtr.Zero;
        }
    }
}
