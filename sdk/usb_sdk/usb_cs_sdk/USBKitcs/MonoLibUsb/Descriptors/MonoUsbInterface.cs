﻿
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace USBKitcs.MonoLibUsb.Descriptors
{
    ///<summary>A collection of alternate settings for a particular USB interface.</summary>
    [StructLayout(LayoutKind.Sequential, Pack = MonoUsbApi.LIBUSB_PACK)]
    public class MonoUsbInterface
    {
        ///<summary> Array of interface descriptors. The length of this array is determined by the num_altsetting field.</summary>
        private IntPtr pAltSetting;

        ///<summary> The number of alternate settings that belong to this interface</summary>
        public readonly int num_altsetting;


        ///<summary> Array of interface descriptors. The length of this array is determined by the num_altsetting field.</summary>
        public List<MonoUsbAltInterfaceDescriptor> AltInterfaceList
        {
            get
            {
                List<MonoUsbAltInterfaceDescriptor> altInterfaceList = new List<MonoUsbAltInterfaceDescriptor>();
                int iAltInterface;
                for (iAltInterface = 0; iAltInterface < num_altsetting; iAltInterface++)
                {
                    IntPtr pNextInterface = new IntPtr(pAltSetting.ToInt64() + (Marshal.SizeOf(typeof (MonoUsbAltInterfaceDescriptor))*iAltInterface));
                    MonoUsbAltInterfaceDescriptor monoUSBAltInterfaceDescriptor = new MonoUsbAltInterfaceDescriptor();
                    Marshal.PtrToStructure(pNextInterface, monoUSBAltInterfaceDescriptor);

                    altInterfaceList.Add(monoUSBAltInterfaceDescriptor);
                }

                return altInterfaceList;
            }
        }
    }
}