namespace USBKitcs.DeviceNotify.Info
{
    /// <summary> Common interface describing a storage volume arrival or removal notification.
    /// </summary> 
    public interface IVolumeNotifyInfo
    {
        /// <summary>
        /// Under windows, gets the letter representation of the unitmask.
        /// Under linux, gets the full path of the device name.
        /// </summary>
        string Letter { get; }

        ///<summary>
        /// If true, change affects media in drive. If false, change affects physical device or drive.
        ///</summary>
        bool ChangeAffectsMediaInDrive { get; }

        /// <summary>
        /// If True, the indicated logical volume is a network volume
        /// </summary>
        bool IsNetworkVolume { get; }

        /// <summary>
        /// Raw DevBroadcastVolume flags.
        /// </summary>
        short Flags { get; }

        /// <summary>
        /// Gets the bit unit mask of the device. IE (bit 0 = A:, bit 1 = B:, etc..)
        /// </summary>
        int Unitmask { get; }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="VolumeNotifyInfo"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="VolumeNotifyInfo"/>.
        ///</returns>
        string ToString();
    }
}