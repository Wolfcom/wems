
using System;
using USBKitcs.Main;

namespace USBKitcs.DeviceNotify.Info
{
    /// <summary> Common inteface describing USB DEVICE INTERFACE arrival and removal events.
    /// </summary> 
    public interface IUsbDeviceNotifyInfo
    {
        /// <summary>
        /// The symbolc name class for this device.  For more information, see <see cref="UsbSymbolicName"/>.
        /// </summary>
        UsbSymbolicName SymbolicName { get; }

        /// <summary>
        /// Gets the full name of the USB device that caused the notification.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Class Guid of the USB device that caused the notification.
        /// </summary>
        Guid ClassGuid { get; }

        /// <summary>
        /// Parses and returns the VID from the <see cref="Name"/> property.
        /// </summary>
        int IdVendor { get; }

        /// <summary>
        /// Parses and returns the PID from the <see cref="Name"/> property.
        /// </summary>
        int IdProduct { get; }

        /// <summary>
        /// Parses and returns the serial number from the <see cref="Name"/> property.
        /// </summary>
        string SerialNumber { get; }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="UsbDeviceNotifyInfo"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="UsbDeviceNotifyInfo"/>.
        ///</returns>
        string ToString();
    }
}