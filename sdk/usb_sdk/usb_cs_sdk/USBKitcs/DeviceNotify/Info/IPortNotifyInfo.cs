namespace USBKitcs.DeviceNotify.Info
{
    /// <summary> Common interface describing a communication port arrival or removal notification.
    /// </summary> 
    public interface IPortNotifyInfo
    {
        /// <summary>
        /// Under windows, Gets the name of the port that caused the event.
        /// Under windows, Gets the full path of the device caused the event.
        /// </summary>
        string Name { get; }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="PortNotifyInfo"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="PortNotifyInfo"/>.
        ///</returns>
        string ToString();
    }
}