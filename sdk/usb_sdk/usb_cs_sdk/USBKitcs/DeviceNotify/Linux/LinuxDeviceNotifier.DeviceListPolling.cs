﻿using System;
using System.Timers;

using USBKitcs.LudnMonoLibUsb;
using USBKitcs.MonoLibUsb.Profile;

namespace USBKitcs.DeviceNotify.Linux
{
    public partial class LinuxDeviceNotifier
    {
        ///<summary>
        /// The interval (milliseconds) in which the device list is queried for changes when using the <see cref="LinuxDeviceNotifierMode.PollDeviceList"/> mode.
        ///</summary>
        public static int PollingInterval = 750;

        private Timer mDeviceListPollTimer;
        private object PollTimerLock = new object();

        private void StartDeviceListPolling()
        {
            lock (PollTimerLock)
            {
                if (mDeviceListPollTimer != null) return;

                MonoUsbDevice.RefreshProfileList();

                MonoUsbDevice.ProfileList.AddRemoveEvent += OnAddRemoveEvent;
                mDeviceListPollTimer = new Timer(PollingInterval);
                mDeviceListPollTimer.Elapsed += PollTimer_Elapsed;
                mDeviceListPollTimer.Start();
            }
        }

        private void PollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (PollTimerLock)
            {
                mDeviceListPollTimer.Stop();
                MonoUsbDevice.RefreshProfileList();
                mDeviceListPollTimer.Start();
            }
        }

        private void StopDeviceListPolling()
        {
            lock (PollTimerLock)
            {
                if (mDeviceListPollTimer == null) return;
                mDeviceListPollTimer.Stop();
                mDeviceListPollTimer.Elapsed -= PollTimer_Elapsed;
                mDeviceListPollTimer.Dispose();
                MonoUsbDevice.ProfileList.AddRemoveEvent -= OnAddRemoveEvent;
                mDeviceListPollTimer = null;
            }
        }


        private void OnAddRemoveEvent(object sender, AddRemoveEventArgs e)
        {
            EventHandler<DeviceNotifyEventArgs> deviceNotify = OnDeviceNotify;
            if (!ReferenceEquals(deviceNotify, null))
            {
                string deviceFileName = String.Format("usbdev{0}.{1}", e.MonoUSBProfile.BusNumber, e.MonoUSBProfile.DeviceAddress);

                LinuxDevItem linuxDevItem = new LinuxDevItem(deviceFileName,
                                                             e.MonoUSBProfile.BusNumber,
                                                             e.MonoUSBProfile.DeviceAddress,
                                                             e.MonoUSBProfile.DeviceDescriptor);

                deviceNotify(this,
                             new LinuxDeviceNotifyEventArgs(linuxDevItem,
                                                            DeviceType.DeviceInterface,
                                                            e.EventType == AddRemoveType.Added
                                                                ? EventType.DeviceArrival
                                                                : EventType.DeviceRemoveComplete));
            }
        }
    }
}