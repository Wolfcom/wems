 
using System;

using USBKitcs.Internal;
using USBKitcs.Main;
using USBKitcs.LudnMonoLibUsb.Internal;
using USBKitcs.MonoLibUsb;

namespace USBKitcs.LudnMonoLibUsb
{
    /// <summary>
    /// Implements mono-linux libusb 1.x methods for reading data from a <see cref="EndpointType.Bulk"/> or <see cref="EndpointType.Interrupt"/> endpoints.
    /// </summary> 
    public class MonoUsbEndpointReader : UsbEndpointReader
    {
        private MonoUsbTransferContext mMonoTransferContext;

        internal MonoUsbEndpointReader(UsbDevice usbDevice, int readBufferSize, ReadEndpointID readEndpointID, EndpointType endpointType)
            : base(usbDevice, readBufferSize, readEndpointID, endpointType) { }

        /// <summary>
        /// Frees resources associated with the endpoint.  Once disposed this class cannot be used.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            if (ReferenceEquals(mMonoTransferContext, null)) return;
            mMonoTransferContext.Dispose();
            mMonoTransferContext = null;
        }

        /// <summary>
        /// Calling this methods is that same as calling <see cref="UsbEndpointReader.ReadFlush"/>
        /// </summary>
        /// <returns>True an success.</returns>
        public override bool Flush()
        {
            if (IsDisposed) throw new ObjectDisposedException(GetType().Name);
            return ReadFlush() == ErrorCode.Success;
        }

        /// <summary>
        /// Cancels pending transfers and clears the halt condition on an enpoint.
        /// </summary>
        /// <returns>True on success.</returns>
        public override bool Reset()
        {
            if (IsDisposed) throw new ObjectDisposedException(GetType().Name);
            Abort();
            int ret = MonoUsbApi.ClearHalt((MonoUsbDeviceHandle) Device.Handle, EpNum);
            if (ret < 0)
            {
                UsbError.Error(ErrorCode.MonoApiError, ret, "Endpoint Reset Failed", this);
                return false;
            }
            return true;
        }


        internal override UsbTransfer CreateTransferContext() { return new MonoUsbTransferContext(this); }
    }
}