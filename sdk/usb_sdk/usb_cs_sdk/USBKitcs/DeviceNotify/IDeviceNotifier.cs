
using System;

namespace USBKitcs.DeviceNotify
{
    /// <summary>
    /// Notifies an application of a change to the hardware Configuration of a device or 
    /// the computer.
    /// </summary>
    /// <remarks>
    /// For devices that offer software-controllable features, such as ejection and locking, 
    /// the system typically sends a <see cref="EventType.DeviceRemovePending"/> message to 
    /// let applications and device drivers end their use of the device gracefully. If the 
    /// system forcibly removes a device, it may not send a 
    /// <see cref="EventType.DeviceQueryRemove"/> message before doing so.
    /// </remarks>
    /// <example>
    /// <code source="..\Test_DeviceNotify\TestDeviceNotify.cs" lang="cs"/>
    /// </example>
    public interface IDeviceNotifier
    {
        ///<summary>
        /// Enables/Disables notification events.
        ///</summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Main Notify event for all device notifications.
        /// </summary>
        event EventHandler<DeviceNotifyEventArgs> OnDeviceNotify;
    }
}