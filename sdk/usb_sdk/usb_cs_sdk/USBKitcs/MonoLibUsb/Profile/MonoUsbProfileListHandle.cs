
using System;
using System.Collections;
using System.Collections.Generic;

using USBKitcs.Main;

namespace USBKitcs.MonoLibUsb.Profile
{
    /// <summary>
    /// Used to iterate through the <see cref="MonoUsbProfileHandle"/> collection contained in the <see cref="MonoUsbProfileListHandle"/>.
    /// </summary>
    /// <remarks>
    /// <para>Wraps a device list handle into a <see cref="System.Runtime.ConstrainedExecution.CriticalFinalizerObject"/></para>
    /// </remarks>
    /// <seealso cref="MonoUsbProfileList"/>
    public class MonoUsbProfileListHandle : SafeContextHandle, IEnumerable<MonoUsbProfileHandle>
    {
        private MonoUsbProfileListHandle()
            : base(IntPtr.Zero) { }

        internal MonoUsbProfileListHandle(IntPtr pHandleToOwn)
            : base(pHandleToOwn) { }

        #region IEnumerable<MonoUsbProfileHandle> Members

        /// <summary>
        /// Gets a forward-only device list enumerator.
        /// </summary>
        /// <returns>A profile handle enumerator used iterating through the <see cref="MonoUsbProfileHandle"/> classes.</returns>
        public IEnumerator<MonoUsbProfileHandle> GetEnumerator() { return new MonoUsbProfileHandleEnumerator(this); }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

        #endregion

        /// <summary>
        /// When overridden in a derived class, executes the code required to free the handle.
        /// </summary>
        /// <returns>
        /// true if the handle is released successfully; otherwise, in the event of a catastrophic failure, false. In this case, it generates a ReleaseHandleFailed Managed Debugging Assistant.
        /// </returns>
        protected override bool ReleaseHandle()
        {
            if (!IsInvalid)
            {
                MonoUsbApi.FreeDeviceList(handle, 1);
                //Console.WriteLine("FreeDeviceList:{0}", handle);
                SetHandleAsInvalid();
            }

            return true;
        }
    }
}