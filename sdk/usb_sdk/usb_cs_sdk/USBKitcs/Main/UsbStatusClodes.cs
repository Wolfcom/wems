
namespace USBKitcs.Main
{
    internal enum UsbStatusClodes
    {
        /// <summary>
        /// Returned bo overlapped IO functions when data is still pending.
        /// </summary>
        ErrorIoPending = 997
    }
}