
namespace USBKitcs.LibUsb
{
    /// <summary>
    /// Kernel types supported by LibUsbDotNet.  See <see cref="UsbDevice.KernelType"/> for more details.
    /// </summary>
    public enum LibUsbKernelType
    {
        /// <summary>
        /// LibUsb support us unavailable.
        /// </summary>
        Unknown,
        /// <summary>
        /// LibUsbDotNet native kernel driver detected.
        /// </summary>
        NativeLibUsb,
        /// <summary>
        /// Original libusb-win32 kernel driver detected.
        /// </summary>
        LegacyLibUsb,
        /// <summary>
        /// mono-linux libusb 1.x driver detected.
        /// </summary>
        MonoLibUsb
    }
}