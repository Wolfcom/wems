using System.Text.RegularExpressions;

namespace USBKitcs.Internal.UsbRegex
{
    internal class BaseRegSymbolicName : Regex
    {
        private const RegexOptions OPTIONS =
            RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled |
            RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase;

        private const string PATTERN =
            @"((&){0,1}Vid_(?<Vid>[0-9A-Fa-f]{1,4})(&){0,1}Pid_(?<Pid>[0-9A-Fa-f]{1,4})((&){0,1}Rev_(?<Rev>[0-9A-Fa-f]{1,4})){0,1})((\x23{0,1}\{(?<ClassGuid>([0-9A-Fa-f]+)-([0-9A-Fa-f]+)-([0-9A-Fa-f]+)-([0-9A-Fa-f]+)-([0-9A-Fa-f]+))})|(\x23(?<String>[\x20-\x22\x24-\x2b\x2d-\x7f]+?)(?=\x23|$)))*";

        public BaseRegSymbolicName() : base(PATTERN, OPTIONS) { }
    }
}