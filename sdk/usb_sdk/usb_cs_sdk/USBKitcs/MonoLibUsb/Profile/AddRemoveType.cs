
namespace USBKitcs.MonoLibUsb.Profile
{
    /// <summary>
    /// Event type.
    /// </summary>
    public enum AddRemoveType
    {
        /// <summary>
        /// A usb device was attached.
        /// </summary>
        Added,
        /// <summary>
        /// A usb device was detached.
        /// </summary>
        Removed
    }
}