using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("USBKitcs")]
[assembly: AssemblyDescription("C# .NET API for USB device low-level USB device communications.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Wolfcom")]
[assembly: AssemblyProduct("USBKitcs")]
[assembly: AssemblyCopyright("Copyright � 2016 Wolfom - Extension of WinUSB and LibUSB.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("0729ae76-a75d-438c-9d72-4bd3bff7c700")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("2.2.8.104")]
[assembly: AssemblyFileVersion("2.2.8.104")]
[assembly: CLSCompliant(true)]
[assembly: NeutralResourcesLanguage("en-US")]