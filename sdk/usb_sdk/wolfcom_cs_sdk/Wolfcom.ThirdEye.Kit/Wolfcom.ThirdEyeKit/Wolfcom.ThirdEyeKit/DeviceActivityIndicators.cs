﻿namespace Wolfcom.ThirdEyeKit
{
    public enum DeviceActivityIndicators
    {
        Flash, Off, On
    }
}
