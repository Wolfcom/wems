﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using USBKitcs.DeviceNotify;

namespace USBKitcs.TestUi.ShowDeviceNotificationEvents
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {

        public IDeviceNotifier UsbDeviceNotifier = DeviceNotifier.OpenDeviceNotifier();

        public MainWindowView()
        {
            InitializeComponent();

            // Hook the device notifier event
            UsbDeviceNotifier.OnDeviceNotify += OnDeviceNotifyEvent;
        }

 



        private void OnDeviceNotifyEvent(object sender, DeviceNotifyEventArgs e)
        {
            // A Device system-level event has occured
            ListViewItem lstViewItem = new ListViewItem();

            lstViewItem.Content = e.ToString();
            listView.Items.Add(lstViewItem);         
        }

        private void listViewCamMediaTransferStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }



        private void button_ClickMount(object sender, RoutedEventArgs e)
        {

        }

        private void button_ClickMedia(object sender, RoutedEventArgs e)
        {

        }

        private void button_ClickCommit(object sender, RoutedEventArgs e)
        {

        }


        private void button_ClickExit(object sender, RoutedEventArgs e)
        {
            /** TODO: Check for unsaved/uncomitted changes for proper closing */
            this.Close();
        }

        private void listViewCamSettings_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
