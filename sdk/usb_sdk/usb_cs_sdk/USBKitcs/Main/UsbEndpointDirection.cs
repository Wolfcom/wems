
using System;

namespace USBKitcs.Main
{
    ///<summary>Endpoint direction.</summary>
    /// <seealso cref="UsbCtrlFlags"/>
    [Flags]
    public enum UsbEndpointDirection : byte
    {
        /// <summary>
        /// In Direction
        /// </summary>
        EndpointIn = 0x80,
        /// <summary>
        /// Out Direction
        /// </summary>
        EndpointOut = 0x00,
    }
}