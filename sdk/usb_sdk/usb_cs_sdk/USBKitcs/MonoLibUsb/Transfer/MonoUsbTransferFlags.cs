
namespace USBKitcs.MonoLibUsb.Transfer
{
    /// <summary>
    /// Transfer flags.
    /// </summary>
    public enum MonoUsbTransferFlags : byte
    {
        /// <summary>
        /// No transfer flags.
        /// </summary>
        None = 0,
        /// <summary>
        /// Report short frames as errors
        /// </summary>
        TransferShortNotOk = 1 << 0,

        /// <summary>
        /// Automatically free() transfer buffer during <see cref ="MonoUsbTransfer.Free">MonoUsbTransfer.Free()</see>.
        /// </summary>
        TransferFreeBuffer = 1 << 1,

        /// <summary>
        /// Automatically call <see cref ="MonoUsbTransfer.Free">MonoUsbTransfer.Free()</see> after callback returns.
        /// </summary>
        /// <remakrks>
        /// <para>If this flag is set, it is illegal to call <see cref ="MonoUsbTransfer.Free"/> from your transfer callback, as this will result in a double-free when this flag is acted upon.</para>
        /// </remakrks>
        TransferFreeTransfer = 1 << 2,
    } ;
}