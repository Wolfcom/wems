﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using USBKitcs;
using USBKitcs.DeviceNotify;
using USBKitcs.Main;


namespace Wolfcom.ThirdEyeKit
{
    public class ThirdEyeManager
    {
        private IDeviceNotifier _deviceNotifier = DeviceNotifier.OpenDeviceNotifier();
        private IUsbDeviceEventDetector _usbNotifier = new UsbDeviceEventDetector();
        private List<ThirdEyeDevice> _devices;

        public ThirdEyeDevice[] Devices
        {
            get
            {
                return this._devices.ToArray();
            }
        }

        public event EventHandler DeviceInserted;

        public event EventHandler<DeviceMountedEventArgs> DeviceMounted;
        
        public event EventHandler<DeviceMountedEventArgs> DeviceDismounted;

        public event EventHandler DeviceRemoved;

        public ThirdEyeManager()
        {
            this._devices = new List<ThirdEyeDevice>();
            this.InitNotifiers();
        }

        private void InitNotifiers()
        {
            this._usbNotifier.DriveInserted += (EventHandler<DriveChangedEventArgs>)((s, e) => this.DeviceMountNotified(e.Drive));
            this._usbNotifier.DriveRemoved += (EventHandler<DriveChangedEventArgs>)((s, e) => this.DeviceDismountNotified(e.Drive));
            this._deviceNotifier = DeviceNotifier.OpenDeviceNotifier();
            this._deviceNotifier.OnDeviceNotify += new EventHandler<DeviceNotifyEventArgs>(this.DeviceNotified);
        }

        private void DeviceNotified(object sender, DeviceNotifyEventArgs e)
        {
            switch (e.EventType)
            {
                case EventType.DeviceArrival:
                    string[] deviceLocations = this._devices
                        .Select<ThirdEyeDevice, string>((Func<ThirdEyeDevice, string>)(x => x.Location))
                            .ToArray<string>();
                    Array.ForEach<UsbThirdEyeDevice>(UsbDevice.AllLibUsbDevices.Where<UsbRegistry>((Func<UsbRegistry, bool>)(x => !((IEnumerable<string>)deviceLocations)
                        .Contains<string>(x.DeviceProperties["LocationInformation"].ToString())))
                            .Select<UsbRegistry, UsbThirdEyeDevice>((Func<UsbRegistry, UsbThirdEyeDevice>)(x => new UsbThirdEyeDevice(x)))
                                .ToArray<UsbThirdEyeDevice>(), new Action<UsbThirdEyeDevice>(this.CreateDevice));
                    break;
                case EventType.DeviceRemoveComplete:
                    string[] usbLocations = UsbDevice.AllLibUsbDevices
                        .Select<UsbRegistry, string>((Func<UsbRegistry, string>)(x => x.DeviceProperties["LocationInformation"].ToString()))
                            .ToArray<string>();
                    Array.ForEach<ThirdEyeDevice>(this._devices.Where<ThirdEyeDevice>((Func<ThirdEyeDevice, bool>)(x => !((IEnumerable<string>)usbLocations)
                        .Contains<string>(x.Location))).ToArray<ThirdEyeDevice>(), new Action<ThirdEyeDevice>(this.RemoveDevice));
                    break;
            }
        }

        private void DeviceMountNotified(string drive)
        {
            if (this.DeviceMounted == null || !Directory.Exists(drive))
                return;
            this.DeviceMounted((object)this, new DeviceMountedEventArgs(drive));
        }

        private void DeviceDismountNotified(string drive)
        {
            if (this.DeviceDismounted == null)
                return;
            this.DeviceDismounted((object)this, new DeviceMountedEventArgs(drive));
        }

        private void RemoveDevices()
        {
            ((IEnumerable<ThirdEyeDevice>)this.Devices)
                .Where<ThirdEyeDevice>((Func<ThirdEyeDevice, bool>)(dev => dev.IsDeviceInserted()))
                    .ToList<ThirdEyeDevice>()
                        .ForEach(new Action<ThirdEyeDevice>(this.RemoveDevice));
        }

        private void RemoveDevice(ThirdEyeDevice device)
        {
            device.Dispose();
            this._devices.Remove(device);
            if (this.DeviceRemoved != null)
                this.DeviceRemoved((object)device, EventArgs.Empty);
            device.FireRemoved();
        }

        public void ScanDevices()
        {
            Array.ForEach<ThirdEyeDevice>(this.Devices, new Action<ThirdEyeDevice>(this.RemoveDevice));
            Array.ForEach<UsbThirdEyeDevice>(UsbThirdEyeDevice.GetThirdEyeDevices(), new Action<UsbThirdEyeDevice>(this.CreateDevice));
        }

        private void CreateDevice(UsbThirdEyeDevice dev)
        {
            ThirdEyeDevice thirdEyeDevice = new ThirdEyeDevice(dev);
            this._devices.Add(thirdEyeDevice);
            if (this.DeviceInserted == null)
                return;
            this.DeviceInserted((object)thirdEyeDevice, EventArgs.Empty);
        }
    }
}
