﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using USBKitcs;
using USBKitcs.Main;

namespace Wolfcom.VisionKit
{
    internal class UsbVisionDevice : IDisposable
    {
        public static UsbDeviceFinder _usbFinder = new UsbDeviceFinder(16981, 1);
        private const int BUF_SIZE = 12288;
        private const uint RAW_READ = 2748;
        private const uint RAW_WRITE = 291;
        private const uint _MAGIC_HEAD_ = 1728199237;
        private const uint _MAGIC_TAIL_ = 2566768058;
        private const int USB_VENDOR_ID = 16981;
        private const int USB_PRODUCT_ID = 1;
        private IUsbDevice _dev;
        private UsbEndpointReader _reader;
        private UsbEndpointWriter _writer;

        internal string DeviceLocation { get; private set; }

        internal UsbVisionDevice(UsbRegistry dev)
        {
            this._dev = dev.Device as IUsbDevice;
            this.DeviceLocation = dev.DeviceProperties["LocationInformation"].ToString();
            this._dev.SetConfiguration((byte)1);
            this._dev.ClaimInterface(0);
            this._reader = this._dev.OpenEndpointReader(ReadEndpointID.Ep01);
            this._writer = this._dev.OpenEndpointWriter(WriteEndpointID.Ep01);
        }

        internal static UsbVisionDevice[] GetVisionDevices()
        {
            return UsbDevice.AllLibUsbDevices.Select<UsbRegistry, UsbVisionDevice>((Func<UsbRegistry, UsbVisionDevice>)(x => new UsbVisionDevice(x))).ToArray<UsbVisionDevice>();
        }

        internal void Release()
        {
            this._dev.Close();
        }

        internal bool IsOpen()
        {
            return this._dev.IsOpen;
        }

        internal string GetDeviceSetup()
        {
            return this.GetDeviceString(UsbVisionDevice.VisionCommand.GET_DEVICE_SETUP);
        }

        internal void SetDeviceSetup(string setup)
        {
            this.SetDeviceString(UsbVisionDevice.VisionCommand.SET_DEVICE_SETUP, setup);
        }

        internal void SetDevicePoweroff()
        {
            this.SetDeviceCommand(UsbVisionDevice.VisionCommand.SET_DEVICE_PASSWORD);
        }

        internal string GetDevicePassword()
        {
            return this.GetDeviceString(UsbVisionDevice.VisionCommand.GET_DEVICE_PASSWORD);
        }

        internal string GetDeviceSerialNo()
        {
            return this.GetDeviceString(UsbVisionDevice.VisionCommand.GET_DEVICE_NUMBER);
        }

        internal string GetDevicePoliceNo()
        {
            return this.GetDeviceString(UsbVisionDevice.VisionCommand.GET_POLICE_NUMBER);
        }

        internal void SetDevicePassword(string password)
        {
            this.SetDeviceString(UsbVisionDevice.VisionCommand.SET_DEVICE_PASSWORD, password);
        }

        internal void SetDeviceSerialNo(string serialNo)
        {
            this.SetDeviceString(UsbVisionDevice.VisionCommand.SET_DEVICE_NUMBER, serialNo);
        }

        internal void SetDevicePoliceNo(string policeNo)
        {
            this.SetDeviceString(UsbVisionDevice.VisionCommand.SET_POLICE_NUMBER, policeNo);
        }

        internal void SetDeviceTime(DateTime time)
        {
            this.SetDeviceCommandData(UsbVisionDevice.VisionCommand.SET_DEVICE_TIME, UsbVisionDevice.GetBytes<UsbVisionDevice.SYSTEMTIME>(new UsbVisionDevice.SYSTEMTIME(time)));
        }

        internal VisionFileInfo GetFileInfo(int index)
        {
            this.SetDeviceInt(UsbVisionDevice.VisionCommand.GET_FILE_INFO_LIST, index);
            return this.GetStruct<VisionFileInfo>(this.GetDeviceData(UsbVisionDevice.VisionCommand.GET_FILE_INFO_LIST));
        }

        internal int GetDeviceFileCount()
        {
            return this.SetDeviceCommand(UsbVisionDevice.VisionCommand.GET_FILES_NUMBER);
        }

        internal void ImportFile(string destination, int index, ulong fileLength, EventHandler<VisionFileImportEventArgs> progress)
        {
            using (FileStream fileStream = new FileStream(destination, FileMode.Create))
            {
                this.SetDeviceInt(UsbVisionDevice.VisionCommand.GET_FILE_INFO_LIST, index);
                ulong currentLength = 0;
                while (currentLength < fileLength)
                {
                    byte[] deviceData = this.GetDeviceData(UsbVisionDevice.VisionCommand.GET_DEVICE_FILE);
                    if (deviceData.Length == 0)
                        break;
                    fileStream.Write(deviceData, 0, deviceData.Length);
                    currentLength += (ulong)deviceData.Length;
                    progress?.Invoke((object)this, new VisionFileImportEventArgs(currentLength));
                }
            }
        }

        internal void SetDeviceMassStorage()
        {
            this.SetDeviceData(BitConverter.GetBytes((short)10));
        }

        private int SetDeviceCommandData(UsbVisionDevice.VisionCommand command, byte[] buf)
        {
            byte[] buf1 = new byte[buf.Length + 1];
            buf1[0] = (byte)command;
            Array.Copy((Array)buf, 0, (Array)buf1, 1, buf.Length);
            return this.SetDeviceData(buf1);
        }

        private int SetDeviceString(UsbVisionDevice.VisionCommand command, string str)
        {
            return this.SetDeviceCommandData(command, Encoding.ASCII.GetBytes(str));
        }

        private string GetDeviceString(UsbVisionDevice.VisionCommand command)
        {
            return Encoding.ASCII.GetString(this.GetDeviceData(command));
        }

        private int SetDeviceCommand(UsbVisionDevice.VisionCommand command)
        {
            return this.SetDeviceCommandData(command, Encoding.ASCII.GetBytes("0000"));
        }

        private void SetDeviceInt(UsbVisionDevice.VisionCommand command, int value)
        {
            this.SetDeviceCommandData(command, BitConverter.GetBytes(value));
        }

        private byte[] GetDeviceData(UsbVisionDevice.VisionCommand command)
        {
            byte[] array = new byte[12288];
            int transferLength = 0;
            this.SendCommand((int)command, UsbVisionDevice.Direction.RAW_READ);
            this.CheckError(this._reader.Read(array, 1000, out transferLength));
            this.CheckResponse(UsbVisionDevice.Direction.RAW_WRITE);
            Array.Resize<byte>(ref array, transferLength);
            return array;
        }

        private int SetDeviceData(byte[] buf)
        {
            int transferLength = 0;
            this.SendCommand(buf.Length, UsbVisionDevice.Direction.RAW_WRITE);
            this.CheckError(this._writer.Write(buf, 1000, out transferLength));
            return this.CheckResponse(UsbVisionDevice.Direction.RAW_READ);
        }

        private void SendCommand(int size, UsbVisionDevice.Direction direction)
        {
            UsbVisionDevice.VisionFlag visionFlag = new UsbVisionDevice.VisionFlag((uint)size, direction);
            int transferLength = 0;
            this.CheckError(this._writer.Write((object)visionFlag, 1000, out transferLength));
        }

        private int CheckResponse(UsbVisionDevice.Direction direction)
        {
            byte[] numArray = new byte[Marshal.SizeOf(typeof(UsbVisionDevice.VisionFlag))];
            int transferLength;
            this.CheckError(this._reader.Read(numArray, 1000, out transferLength));
            UsbVisionDevice.VisionFlag visionFlag = this.GetStruct<UsbVisionDevice.VisionFlag>(numArray);
            if ((UsbVisionDevice.Direction)visionFlag.read_write != direction)
                throw new Exception("Device I/O error: Invalid command response from: " + new StackTrace().GetFrame(3).GetMethod().Name);
            return (int)visionFlag.size;
        }

        private void CheckError(ErrorCode ec)
        {
            if (ec != ErrorCode.None)
                throw new Exception(string.Format("Device error: {0} {1}", (object)ec, (object)UsbDevice.LastErrorString));
        }

        private T GetStruct<T>(byte[] data) where T : struct
        {
            int num1 = Marshal.SizeOf(typeof(T));
            IntPtr num2 = Marshal.AllocHGlobal(num1);
            Array.Resize<byte>(ref data, num1);
            Marshal.Copy(data, 0, num2, data.Length);
            T structure = (T)Marshal.PtrToStructure(num2, typeof(T));
            Marshal.FreeHGlobal(num2);
            return structure;
        }

        public static byte[] GetBytes<T>(T data) where T : struct
        {
            int length = Marshal.SizeOf(typeof(T));
            byte[] destination = new byte[length];
            IntPtr num = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr((object)data, num, true);
            Marshal.Copy(num, destination, 0, length);
            Marshal.FreeHGlobal(num);
            return destination;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;
            this._dev.Close();
        }

        private enum VisionCommand : byte
        {
            DEVICE_COMMAND_ID,
            SET_DEVICE_TIME,
            SET_DEVICE_NUMBER,
            GET_DEVICE_NUMBER,
            SET_POLICE_NUMBER,
            GET_POLICE_NUMBER,
            SET_DEVICE_PASSWORD,
            GET_FILES_NUMBER,
            GET_FILE_INFO_LIST,
            GET_DEVICE_FILE,
            SET_MASSSTORAGE,
            GET_DEVICE_PASSWORD,
            SET_DEVICE_POWEROFF,
            GET_DEVICE_SETUP,
            SET_DEVICE_SETUP,
            SET_DEVICE_DEFAULT,
            SET_DEV_FILE_FORMAT,
            SET_DEVICE_GPS,
        }

        private enum Direction : uint
        {
            RAW_WRITE = 291,
            RAW_READ = 2748,
        }

        private struct VisionFlag
        {
            internal uint magic_head;
            internal uint read_write;
            internal uint size;
            internal uint magic_tail;

            internal VisionFlag(uint size, UsbVisionDevice.Direction direction)
            {
                this.magic_head = 1728199237U;
                this.read_write = (uint)direction;
                this.size = size;
                this.magic_tail = 2566768058U;
            }
        }

        private struct SYSTEMTIME
        {
            [MarshalAs(UnmanagedType.U2)]
            public short Year;
            [MarshalAs(UnmanagedType.U2)]
            public short Month;
            [MarshalAs(UnmanagedType.U2)]
            public short DayOfWeek;
            [MarshalAs(UnmanagedType.U2)]
            public short Day;
            [MarshalAs(UnmanagedType.U2)]
            public short Hour;
            [MarshalAs(UnmanagedType.U2)]
            public short Minute;
            [MarshalAs(UnmanagedType.U2)]
            public short Second;
            [MarshalAs(UnmanagedType.U2)]
            public short Milliseconds;

            public SYSTEMTIME(DateTime dt)
            {
                this.Year = (short)dt.Year;
                this.Month = (short)dt.Month;
                this.DayOfWeek = (short)dt.DayOfWeek;
                this.Day = (short)dt.Day;
                this.Hour = (short)dt.Hour;
                this.Minute = (short)dt.Minute;
                this.Second = (short)dt.Second;
                this.Milliseconds = (short)dt.Millisecond;
            }
        }
    }
}
