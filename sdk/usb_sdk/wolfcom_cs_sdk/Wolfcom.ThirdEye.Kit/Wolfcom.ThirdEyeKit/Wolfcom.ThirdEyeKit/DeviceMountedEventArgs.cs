﻿using System;

namespace Wolfcom.ThirdEyeKit
{
    public class DeviceMountedEventArgs : EventArgs
    {
        public string Drive { get; private set; }

        internal DeviceMountedEventArgs(string drive)
        {
            this.Drive = drive;
        }
    }
}
