using System;
using System.Runtime.InteropServices;
using System.Text;

namespace USBKitcs.Descriptors
{
    internal abstract class UsbMemChunk
    {
        private readonly int mMaxSize;

        private IntPtr mMemPointer = IntPtr.Zero;

        protected UsbMemChunk(int maxSize)
        {
            mMaxSize = maxSize;
            mMemPointer = Marshal.AllocHGlobal(maxSize);
        }

        public int MaxSize
        {
            get { return mMaxSize; }
        }

        public IntPtr Ptr
        {
            get { return mMemPointer; }
        }

        public void Free()
        {
            if (mMemPointer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(mMemPointer);
                mMemPointer = IntPtr.Zero;
            }
        }

        ~UsbMemChunk() { Free(); }
    }

    internal class LangStringDescriptor : UsbMemChunk
    {
        #region FIELD_OFFSETS

        private static readonly int OfsDescriptorType = Marshal.OffsetOf(typeof (UsbDescriptor), "DescriptorType").ToInt32();
        private static readonly int OfsLength = Marshal.OffsetOf(typeof (UsbDescriptor), "Length").ToInt32();

        #endregion

        public LangStringDescriptor(int maxSize)
            : base(maxSize) { }

        public DescriptorType DescriptorType
        {
            get { return (DescriptorType) Marshal.ReadByte(Ptr, OfsDescriptorType); }
            set { Marshal.WriteByte(Ptr, OfsDescriptorType, (byte) value); }
        }

        public byte Length
        {
            get { return Marshal.ReadByte(Ptr, OfsLength); }
            set { Marshal.WriteByte(Ptr, OfsLength, value); }
        }

        public bool Get(out short[] langIds)
        {
            langIds = new short[0];
            int totalLength = Length;
            if (totalLength <= 2) return false;

            int elementCount = (totalLength - 2)/2;
            langIds = new short[elementCount];

            int startOffset = UsbDescriptor.Size;
            for (int iElement = 0; iElement < langIds.Length; iElement++)
            {
                langIds[iElement] = Marshal.ReadInt16(Ptr, startOffset + (sizeof (ushort)*iElement));
            }
            return true;
        }

        public bool Get(out byte[] bytes)
        {
            bytes = new byte[Length];
            Marshal.Copy(Ptr, bytes, 0, bytes.Length);
            return true;
        }

        public bool Get(out string str)
        {
            str = string.Empty;

            byte[] bytes;
            if (Get(out bytes))
            {
                if (bytes.Length <= UsbDescriptor.Size)
                {
                    str = string.Empty;
                }
                else
                {
                    str = Encoding.Unicode.GetString(bytes, UsbDescriptor.Size, bytes.Length - UsbDescriptor.Size);
                }
                return true;
            }

            return false;
        }
    }
}