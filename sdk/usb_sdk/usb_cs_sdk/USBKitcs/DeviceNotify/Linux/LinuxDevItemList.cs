
using System.Collections.Generic;

namespace USBKitcs.DeviceNotify.Linux
{
    internal class LinuxDevItemList : List<LinuxDevItem>
    {
        public LinuxDevItem FindByName(string deviceFileName)
        {
            foreach (LinuxDevItem item in this)
                if (item.DeviceFileName == deviceFileName) return item;

            return null;
        }
    }
}