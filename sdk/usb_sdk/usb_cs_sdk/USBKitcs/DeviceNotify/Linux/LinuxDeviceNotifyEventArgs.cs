﻿
using System;

namespace USBKitcs.DeviceNotify.Linux
{
    /// <summary>
    /// Describes the device notify event
    /// </summary> 
    public class LinuxDeviceNotifyEventArgs : DeviceNotifyEventArgs
    {
        internal LinuxDeviceNotifyEventArgs(LinuxDevItem linuxDevItem, DeviceType deviceType, EventType eventType)
        {
            mEventType = eventType;
            mDeviceType = deviceType;
            switch (mDeviceType)
            {
                case DeviceType.Volume:
                    throw new NotImplementedException(mDeviceType.ToString());
                case DeviceType.Port:
                    throw new NotImplementedException(mDeviceType.ToString());
                case DeviceType.DeviceInterface:
                    mDevice = new LinuxUsbDeviceNotifyInfo(linuxDevItem);
                    mObject = mDevice;
                    break;
            }
        }

        //internal LinuxDeviceNotifyEventArgs(DevBroadcastHdr hdr, IntPtr ptrHdr, EventType eventType)
        //{
        //    mBaseHdr = hdr;
        //    mEventType = eventType;
        //    mDeviceType = mBaseHdr.DeviceType;
        //    switch (mDeviceType)
        //    {
        //        case DeviceType.Volume:
        //            mVolume = new VolumeNotifyInfo(ptrHdr);
        //            mObject = mVolume;
        //            break;
        //        case DeviceType.Port:
        //            mPort = new PortNotifyInfo(ptrHdr);
        //            mObject = mPort;
        //            break;
        //        case DeviceType.DeviceInterface:
        //            mDevice = new UsbDeviceNotifyInfo(ptrHdr);
        //            mObject = mDevice;
        //            break;
        //    }
        //}
    }
}