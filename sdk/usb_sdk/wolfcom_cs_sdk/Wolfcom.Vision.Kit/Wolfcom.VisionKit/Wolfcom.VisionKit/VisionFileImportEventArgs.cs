﻿using System;

namespace Wolfcom.VisionKit
{
    public class VisionFileImportEventArgs : EventArgs
    {
        public readonly ulong CurrentLength;

        internal VisionFileImportEventArgs (ulong currentLength)
        {
            CurrentLength = currentLength;

        }
    }
}
