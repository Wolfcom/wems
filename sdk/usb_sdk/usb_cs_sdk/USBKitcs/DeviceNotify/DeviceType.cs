namespace USBKitcs.DeviceNotify
{
    /// <summary> 
    /// Type of notification device.
    /// </summary> 
    public enum DeviceType
    {
        /// <summary>
        /// oem-defined device type.
        /// </summary>
        Oem = 0x00000000,
        /// <summary>
        /// devnode number.
        /// </summary>
        DevNode = 0x00000001,
        /// <summary>
        /// logical volume.
        /// </summary>
        Volume = 0x00000002,
        /// <summary>
        /// serial, parallel.
        /// </summary>
        Port = 0x00000003,
        /// <summary>
        /// network resource.
        /// </summary>
        Net = 0x00000004,
        /// <summary>
        /// device interface class
        /// </summary>
        DeviceInterface = 0x00000005,
        /// <summary>
        /// file system handle.
        /// </summary>
        Handle = 0x00000006
    }
}