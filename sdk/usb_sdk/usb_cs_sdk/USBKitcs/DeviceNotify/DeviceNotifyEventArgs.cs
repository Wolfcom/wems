using System;
using USBKitcs.DeviceNotify.Info;

namespace USBKitcs.DeviceNotify
{
    /// <summary>
    /// Describes the device notify event
    /// </summary> 
    public abstract class DeviceNotifyEventArgs : EventArgs
    {
        internal IUsbDeviceNotifyInfo mDevice;
        internal DeviceType mDeviceType;
        internal EventType mEventType;
        internal object mObject;
        internal IPortNotifyInfo mPort;
        internal IVolumeNotifyInfo mVolume;

        /// <summary>
        /// Gets the <see cref="VolumeNotifyInfo"/> class.
        /// </summary>
        /// <remarks>
        /// This value is null if the <see cref="DeviceNotifyEventArgs.DeviceType"/> is not set to <see cref="DeviceNotify.DeviceType.Volume"/>
        /// </remarks>
        public IVolumeNotifyInfo Volume
        {
            get { return mVolume; }
        }

        /// <summary>
        /// Gets the <see cref="PortNotifyInfo"/> class.
        /// </summary>
        /// <remarks>
        /// This value is null if the <see cref="DeviceNotifyEventArgs.DeviceType"/> is not set to <see cref="DeviceNotify.DeviceType.Port"/>
        /// </remarks>
        public IPortNotifyInfo Port
        {
            get { return mPort; }
        }

        /// <summary>
        /// Gets the <see cref="UsbDeviceNotifyInfo"/> class.
        /// </summary>
        /// <remarks>
        /// This value is null if the <see cref="DeviceNotifyEventArgs.DeviceType"/> is not set to <see cref="DeviceNotify.DeviceType.DeviceInterface"/>
        /// </remarks>
        public IUsbDeviceNotifyInfo Device
        {
            get { return mDevice; }
        }

        /// <summary>
        /// Gets the <see cref="EventType"/> for this notification.
        /// </summary>
        public EventType EventType
        {
            get { return mEventType; }
        }

        /// <summary>
        /// Gets the <see cref="DeviceType"/> for this notification.
        /// </summary>
        public DeviceType DeviceType
        {
            get { return mDeviceType; }
        }

        /// <summary>
        /// Gets the notification class as an object.
        /// </summary>
        /// <remarks>
        /// This value is never null.
        /// </remarks>
        public object Object
        {
            get { return mObject; }
        }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="DeviceNotifyEventArgs"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="DeviceNotifyEventArgs"/>.
        ///</returns>
        public override string ToString()
        {
            object[] o = {DeviceType, EventType, mObject.ToString()};
            return string.Format("[DeviceType:{0}] [EventType:{1}] {2}", o);
        }
    }
}