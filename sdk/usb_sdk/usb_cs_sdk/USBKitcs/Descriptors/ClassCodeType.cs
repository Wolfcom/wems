﻿using System;

namespace USBKitcs.Descriptors
{
    ///<summary> Device and/or Interface Class codes</summary>
    [Flags]
    public enum ClassCodeType : byte
    {
        ///<summary>In the context of a "device descriptor", this bDeviceClass value indicates that each interface specifies its own class information and all interfaces operate independently.</summary>
        PerInterface = 0,

        ///<summary>Audio class</summary>
        Audio = 1,

        ///<summary> Communications class</summary>
        Comm = 2,

        ///<summary> Human Interface Device class</summary>
        Hid = 3,

        ///<summary> Printer dclass</summary>
        Printer = 7,

        ///<summary> Picture transfer protocol class</summary>
        Ptp = 6,

        ///<summary> Mass storage class</summary>
        MassStorage = 8,

        ///<summary> Hub class</summary>
        Hub = 9,

        ///<summary> Data class</summary>
        Data = 10,

        ///<summary> Class is vendor-specific</summary>
        VendorSpec = 0xff
    }
}