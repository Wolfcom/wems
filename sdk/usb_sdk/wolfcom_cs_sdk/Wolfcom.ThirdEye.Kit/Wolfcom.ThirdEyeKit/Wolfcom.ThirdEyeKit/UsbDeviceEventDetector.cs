﻿using System;
using System.Runtime.InteropServices;

namespace Wolfcom.ThirdEyeKit
{
    internal class UsbDeviceEventDetector : IUsbDeviceEventDetector, IDisposable
    {
        private const int WM_DEVICECHANGE = 537;
        private const int DBT_DEVICEARRIVAL = 32768;
        private const int DBT_DEVICEREMOVECOMPLETE = 32772;
        private const int DBT_DEVNODES_CHANGED = 7;
        private const int DBT_DEVTYP_VOLUME = 2;
        private readonly string _className;
        private readonly IntPtr _windowHandle;
        private static UsbDeviceEventDetector.WndProc noGCthis;


        public event EventHandler<EventArgs> DeviceInsertedOrRemoved;

        public event EventHandler<DriveChangedEventArgs> DriveInserted;

        public event EventHandler<DriveChangedEventArgs> DriveRemoved;




        internal UsbDeviceEventDetector()
        {
            UsbDeviceEventDetector.WNDCLASS wc = new UsbDeviceEventDetector.WNDCLASS();
            UsbDeviceEventDetector.noGCthis = new UsbDeviceEventDetector.WndProc(this.WndProcCallback);
            wc.lpfnWndProc = UsbDeviceEventDetector.noGCthis;
            this._className = "UsbDeviceEventDetector" + (object)new Random().Next();
            wc.lpszClassName = this._className;
            int num = (int)UsbDeviceEventDetector.RegisterClass(wc);
            this._windowHandle = UsbDeviceEventDetector.CreateWindowEx(0, wc.lpszClassName, "Window title", 0, 100, 100, 500, 500, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, (object)0);
        }





        [DllImport("User32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CreateWindowEx(int dwExStyle, string lpszClassName, string lpszWindowName, int style, int x, int y, int width, int height, IntPtr hWndParent, IntPtr hMenu, IntPtr hInst, [MarshalAs(UnmanagedType.AsAny)] object pvParam);

        [DllImport("user32.dll")]
        private static extern IntPtr DefWindowProc(IntPtr hWnd, int uMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern short RegisterClass(UsbDeviceEventDetector.WNDCLASS wc);

        [DllImport("User32", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern short UnRegisterClass(string className, IntPtr handle);

        [AllowReversePInvokeCalls]
        private IntPtr WndProcCallback(IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam)
        {
            if (msg == 537)
            {
                switch (wparam.ToInt32())
                {
                    case 7:
                        this.FireDeviceInsertedOrRemoved();
                        break;
                    case 32768:
                        string drive1 = this.GetDrive(lparam);
                        if (drive1.Length > 0)
                        {
                            this.FireDriveInserted(drive1);
                            break;
                        }
                        break;
                    case 32772:
                        string drive2 = this.GetDrive(lparam);
                        if (drive2.Length > 0)
                        {
                            this.FireDriveRemoved(drive2);
                            break;
                        }
                        break;
                }
            }
            return UsbDeviceEventDetector.DefWindowProc(hWnd, msg, wparam, lparam);
        }

        private string GetDrive(IntPtr lparam)
        {
            UsbDeviceEventDetector.DEV_BROADCAST_VOLUME devBroadcastVolume = new UsbDeviceEventDetector.DEV_BROADCAST_VOLUME();
            Marshal.PtrToStructure(lparam, (object)devBroadcastVolume);
            if ((int)devBroadcastVolume.dbch_Devicetype == 2)
            {
                ulong dbchUnitmask = (ulong)devBroadcastVolume.dbch_Unitmask;
                char ch = 'A';
                while ((int)ch <= 90)
                {
                    if (((long)dbchUnitmask & 1L) != 0L)
                        return string.Format("{0}:\\", (object)ch);
                    ++ch;
                    dbchUnitmask >>= 1;
                }
            }
            return string.Empty;
        }

        private void FireDriveInserted(string drive)
        {
            if (this.DriveInserted == null)
                return;
            this.DriveInserted((object)this, new DriveChangedEventArgs(drive));
        }

        private void FireDriveRemoved(string drive)
        {
            if (this.DriveRemoved == null)
                return;
            this.DriveRemoved((object)this, new DriveChangedEventArgs(drive));
        }

        private void FireDeviceInsertedOrRemoved()
        {
            if (this.DeviceInsertedOrRemoved == null)
                return;
            this.DeviceInsertedOrRemoved((object)this, EventArgs.Empty);
        }




        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                int num = (int)UsbDeviceEventDetector.UnRegisterClass(this._className, this._windowHandle);

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UsbDeviceEventDetector() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion



        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class WNDCLASS
        {
            public int style;
            public UsbDeviceEventDetector.WndProc lpfnWndProc;
            public int cbClsExtra;
            public int cbWndExtra;
            public IntPtr hInstance;
            public IntPtr hIcon;
            public IntPtr hCursor;
            public IntPtr hbrBackground;
            public string lpszMenuName;
            public string lpszClassName;
        }

        [StructLayout(LayoutKind.Sequential)]
        private class DEV_BROADCAST_VOLUME
        {
            public uint dbch_Size;
            public uint dbch_Devicetype;
            public uint dbch_Reserved;
            public uint dbch_Unitmask;
            public ushort dbch_Flags;
        }

        public delegate IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);
    }

}

