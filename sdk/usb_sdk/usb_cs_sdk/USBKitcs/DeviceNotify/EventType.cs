
namespace USBKitcs.DeviceNotify
{
    /// <summary> 
    /// Type of notification event.
    /// </summary> 
    public enum EventType
    {
        /// <summary>
        /// A custom event has occurred.
        /// </summary>
        CustomEvent = 0x8006,
        /// <summary>
        /// A device or piece of media has been inserted and is now available.
        /// </summary>
        DeviceArrival = 0x8000,
        /// <summary>
        /// Permission is requested to remove a device or piece of media. Any application can deny this request and cancel the removal.
        /// </summary>
        DeviceQueryRemove = 0x8001,
        /// <summary>
        /// A request to remove a device or piece of media has been canceled.
        /// </summary>
        DeviceQueryRemoveFailed = 0x8002,
        /// <summary>
        /// A device or piece of media has been removed.
        /// </summary>
        DeviceRemoveComplete = 0x8004,
        /// <summary>
        /// A device or piece of media is about to be removed. Cannot be denied.
        /// </summary>
        DeviceRemovePending = 0x8003,
        /// <summary>
        /// A device-specific event has occurred.
        /// </summary>
        DeviceTypeSpecific = 0x8005
    }
}