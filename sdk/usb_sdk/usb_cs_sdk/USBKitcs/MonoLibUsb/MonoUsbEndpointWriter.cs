
using System;

using USBKitcs.Internal;
using USBKitcs.Main;
using USBKitcs.LudnMonoLibUsb.Internal;
using USBKitcs.MonoLibUsb;

namespace USBKitcs.LudnMonoLibUsb
{
    /// <summary>
    /// Implements mono-linux libusb 1.x methods for writing to methods for writing data to a <see cref="EndpointType.Bulk"/> or <see cref="EndpointType.Interrupt"/> endpoints.
    /// </summary> 
    public class MonoUsbEndpointWriter : UsbEndpointWriter
    {
        private MonoUsbTransferContext mMonoTransferContext;

        internal MonoUsbEndpointWriter(UsbDevice usbDevice, WriteEndpointID writeEndpointID,EndpointType endpointType)
            : base(usbDevice, writeEndpointID, endpointType) { }

        /// <summary>
        /// Frees resources associated with the endpoint.  Once disposed this class cannot be used.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();
            if (ReferenceEquals(mMonoTransferContext, null)) return;
            mMonoTransferContext.Dispose();
            mMonoTransferContext = null;
        }

        /// <summary>
        /// This method has no effect on write endpoints, andalways returs true.
        /// </summary>
        /// <returns>True</returns>
        public override bool Flush() { return true; }

        /// <summary>
        /// Cancels pending transfers and clears the halt condition on an enpoint.
        /// </summary>
        /// <returns>True on success.</returns>
        public override bool Reset()
        {
            if (IsDisposed) throw new ObjectDisposedException(GetType().Name);
            Abort();
            int ret = MonoUsbApi.ClearHalt((MonoUsbDeviceHandle) Device.Handle, EpNum);
            if (ret < 0)
            {
                UsbError.Error(ErrorCode.MonoApiError, ret, "Endpoint Reset Failed", this);
                return false;
            }
            return true;
        }

        internal override UsbTransfer CreateTransferContext() { return new MonoUsbTransferContext(this); }
    }
}