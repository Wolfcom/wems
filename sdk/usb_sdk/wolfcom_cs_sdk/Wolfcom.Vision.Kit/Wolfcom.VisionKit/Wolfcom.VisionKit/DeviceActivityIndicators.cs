﻿namespace Wolfcom.VisionKit
{
    public enum DeviceActivityIndicators
    {
        Flash, Off, On
    }
}
