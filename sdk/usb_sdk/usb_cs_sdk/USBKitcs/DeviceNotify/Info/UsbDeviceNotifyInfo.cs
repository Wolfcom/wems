using System;
using System.Runtime.InteropServices;
using USBKitcs.DeviceNotify.Internal;
using USBKitcs.Main;

namespace USBKitcs.DeviceNotify.Info
{
    /// <summary> Describes the USB device that caused the notification.
    /// See the <see cref="IUsbDeviceNotifyInfo"/> inteface for more information.
    /// </summary>  
    public class UsbDeviceNotifyInfo : IUsbDeviceNotifyInfo
    {
        private readonly DevBroadcastDeviceinterface mBaseHdr = new DevBroadcastDeviceinterface();
        private readonly string mName;
        private UsbSymbolicName mSymbolicName;

        internal UsbDeviceNotifyInfo(IntPtr lParam)
        {
            Marshal.PtrToStructure(lParam, mBaseHdr);
            IntPtr pName = new IntPtr(lParam.ToInt64() + Marshal.OffsetOf(typeof (DevBroadcastDeviceinterface), "mNameHolder").ToInt64());
            mName = Marshal.PtrToStringAuto(pName);
        }

        #region IUsbDeviceNotifyInfo Members

        /// <summary>
        /// The symbolc name class for this device.  For more information, see <see cref="UsbSymbolicName"/>.
        /// </summary>
        public UsbSymbolicName SymbolicName
        {
            get
            {
                if (ReferenceEquals(mSymbolicName, null))
                    mSymbolicName = new UsbSymbolicName(mName);

                return mSymbolicName;
            }
        }

        /// <summary>
        /// Gets the full name of the USB device that caused the notification.
        /// </summary>
        public string Name
        {
            get { return mName; }
        }

        /// <summary>
        /// Gets the Class Guid of the USB device that caused the notification.
        /// </summary>
        public Guid ClassGuid
        {
            get { return SymbolicName.ClassGuid; }
        }

        /// <summary>
        /// Parses and returns the VID from the <see cref="Name"/> property.
        /// </summary>
        public int IdVendor
        {
            get { return SymbolicName.Vid; }
        }

        /// <summary>
        /// Parses and returns the PID from the <see cref="Name"/> property.
        /// </summary>
        public int IdProduct
        {
            get { return SymbolicName.Pid; }
        }

        /// <summary>
        /// Parses and returns the serial number from the <see cref="Name"/> property.
        /// </summary>
        public string SerialNumber
        {
            get { return SymbolicName.SerialNumber; }
        }

        ///<summary>
        ///Returns a <see cref="T:System.String"/> that represents the current <see cref="UsbDeviceNotifyInfo"/>.
        ///</summary>
        ///
        ///<returns>
        ///A <see cref="System.String"/> that represents the current <see cref="UsbDeviceNotifyInfo"/>.
        ///</returns>
        public override string ToString() { return SymbolicName.ToString(); }

        #endregion
    }
}